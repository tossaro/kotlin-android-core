@file:Suppress("UNUSED")

package multi.platform.core.shared.external.extensions

import multi.platform.core.shared.external.enums.PermissionEnum
import multi.platform.core.shared.external.utilities.permission.PermissionDelegate
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.qualifier.named

internal fun KoinComponent.getPermissionDelegate(permission: PermissionEnum): PermissionDelegate {
    val permissionDelegate by inject<PermissionDelegate>(named(permission.name))
    return permissionDelegate
}
