@file:Suppress("UNUSED", "UNCHECKED_CAST")

package multi.platform.core.shared.app.common

import io.ktor.client.plugins.ResponseException
import io.ktor.client.plugins.ServerResponseException
import io.ktor.utils.io.errors.IOException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import multi.platform.core.shared.domain.common.usecase.CoreUseCase

abstract class ItemViewModel<
    ITEM,
    GET : CoreUseCase,
    READ : CoreUseCase,
    SAVE : CoreUseCase,
    SUBMIT : CoreUseCase,
    DELETE : CoreUseCase,
    REMOVE : CoreUseCase,
    >(
    private val getUseCase: GET,
    private val readUseCase: READ,
    private val saveUseCase: SAVE,
    private val submitUseCase: SUBMIT?,
    private val deleteUseCase: DELETE?,
    private val removeUseCase: REMOVE?,
) :
    CoreViewModel() {
    var item = MutableStateFlow<ITEM?>(null)
    var onDeleted = MutableStateFlow(false)
    var id: Any? = null

    open fun load(isFromNetworkOpt: Boolean = isFromNetwork) {
        if (isFromNetworkOpt) {
            getFromNetwork()
        } else {
            getFromLocal()
        }
    }

    open fun getFromLocal(idArg: Any? = null) {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            val result = readUseCase.call(idArg ?: id) as ITEM?
            result?.let {
                scope.launch {
                    item.value = result
                    loadingIndicator.value = false
                }
            } ?: run {
                getFromNetwork()
            }
        }
    }

    open fun getFromNetwork(idArg: Any? = null) {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            try {
                val response = getUseCase.call(idArg ?: id) as ITEM?
                saveToLocal(response)
                scope.launch { loadingIndicator.value = false }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch {
                    loadingIndicator.value = false
                    when (e) {
                        is ServerResponseException, is IOException -> onException.value = e
                        is ResponseException -> errorResponse.value = e.response
                        else -> errorMessage.value = e.message
                    }
                }
            }
        }
    }

    open suspend fun saveToLocal(newItem: ITEM?) {
        var saved: ITEM? = null
        newItem?.let {
            saved = saveUseCase.call(modifyItemResponse(it)) as ITEM
        }
        scope.launch { item.value = saved }
    }

    open fun modifyItemResponse(item: ITEM) = item

    open fun deleteOnNetwork(idArg: Any? = null) {
        if (deleteUseCase == null) throw UnsupportedOperationException("DeleteUseCase not imported")
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            try {
                val response = deleteUseCase.call(idArg ?: id) as Boolean
                if (response) removeOnLocal(idArg ?: id)
                scope.launch { loadingIndicator.value = false }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch {
                    loadingIndicator.value = false
                    when (e) {
                        is ServerResponseException, is IOException -> onException.value = e
                        is ResponseException -> errorResponse.value = e.response
                        else -> errorMessage.value = e.message
                    }
                }
            }
        }
    }

    open suspend fun removeOnLocal(idArg: Any? = null) {
        if (removeUseCase == null) throw UnsupportedOperationException("RemoveUseCase not imported")
        val result = removeUseCase.call(idArg ?: id) as Boolean
        scope.launch { onDeleted.value = result }
    }

    open fun validateBeforeSubmit(itemArg: Any? = null) = true

    open fun submitToNetwork(itemArg: Any? = null) {
        if (submitUseCase == null) throw UnsupportedOperationException("SubmitUseCase not imported")
        if (!validateBeforeSubmit(itemArg ?: item.value)) return
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            try {
                val response = submitUseCase.call(itemArg ?: item.value) as ITEM?
                saveToLocal(response)
                scope.launch { loadingIndicator.value = false }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch {
                    loadingIndicator.value = false
                    when (e) {
                        is ServerResponseException, is IOException -> onException.value = e
                        is ResponseException -> errorResponse.value = e.response
                        else -> errorMessage.value = e.message
                    }
                }
            }
        }
    }
}
