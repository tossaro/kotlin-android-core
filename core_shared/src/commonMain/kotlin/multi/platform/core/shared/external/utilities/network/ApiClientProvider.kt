package multi.platform.core.shared.external.utilities.network

import io.ktor.http.URLProtocol
import kotlinx.serialization.json.Json
import multi.platform.core.shared.Context
import multi.platform.core.shared.domain.common.usecase.RefreshTokenUseCase
import multi.platform.core.shared.external.CoreConfig
import multi.platform.core.shared.external.utilities.Persistent

interface ApiClientProvider<T> {
    val context: Context?
    val refreshTokenUseCase: RefreshTokenUseCase?
    val json: Json
    val coreConfig: CoreConfig
    val server: String?
    val serverProtocol: URLProtocol?
    val persistent: Persistent
    val deviceId: String
    val version: String
    val client: T
}
