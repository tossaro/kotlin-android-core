package multi.platform.core.shared

import io.ktor.client.engine.HttpClientEngine
import org.koin.core.module.Module

expect class Bundle
expect class Context
expect class SSLPeerUnverifiedException

interface Platform {
    val name: String
    val apiEngine: HttpClientEngine
    fun player(context: Context?, key: String): Any?
}

expect fun getPlatform(): Platform
expect fun getLanguage(context: Context?): String

@OptIn(ExperimentalMultiplatform::class)
@OptionalExpectation
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.BINARY)
expect annotation class Parcelize()

expect interface Parcelable

expect fun formatDate(dateString: String, fromFormat: String, toFormat: String): String

expect fun platformModule(): Module

expect class DecimalFormat() {
    fun format(double: Double, maximumFractionDigits: Int): String
}
