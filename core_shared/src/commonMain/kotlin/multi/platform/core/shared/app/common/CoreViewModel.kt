@file:Suppress("KOTLIN:S6305")

package multi.platform.core.shared.app.common

import io.ktor.client.statement.HttpResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow

expect abstract class CoreViewModel() {
    var errorEmptyField: String?
    var errorMinChar: String?
    var errorPhoneFormat: String?
    var errorEmailFormat: String?
    var errorPasswordFormat: String?
    var errorConfirm: String?
    var accessToken: String?
    val isEmpty: MutableStateFlow<Boolean>
    var isFromNetwork: Boolean
    var useAsyncNetworkCall: Boolean

    val loadingIndicator: MutableStateFlow<Boolean?>
    val toastMessage: MutableStateFlow<String?>
    val successMessage: MutableStateFlow<String?>
    val errorMessage: MutableStateFlow<String?>
    val errorResponse: MutableStateFlow<HttpResponse?>
    val forceSignout: MutableStateFlow<Boolean>
    val onException: MutableStateFlow<Exception?>
    val scope: CoroutineScope

    fun validateBlank(field: MutableStateFlow<String?>, error: MutableStateFlow<String?>): Boolean
    fun validateMinChar(
        min: Int,
        field: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean

    fun validatePhoneFormat(
        field: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean

    fun validateEmailFormat(
        field: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean

    fun validatePasswordFormat(
        field: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean

    fun validateConfirm(
        field1: MutableStateFlow<String?>,
        field2: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean
}
