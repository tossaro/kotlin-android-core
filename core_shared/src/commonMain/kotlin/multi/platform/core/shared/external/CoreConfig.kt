package multi.platform.core.shared.external

interface CoreConfig {
    val apiChannel: String?
    val apiAuthPath: String?
    val apiRefreshTokenPath: String?
    val headerChannel: String?
    val headerDeviceId: String?
    val headerLanguage: String?
    val headerOs: String?
    val headerVersion: String?
}
