@file:Suppress("UNUSED")

package multi.platform.core.shared.domain.common.usecase

interface CoreUseCase {
    suspend fun call(vararg args: Any?): Any?

    /**
     * Override this function for handling any error occurred.
     * Example: Remove saved token and refresh token
     */
    suspend fun onError(e: Exception) {
        throw UnsupportedOperationException("not implemented")
    }
}
