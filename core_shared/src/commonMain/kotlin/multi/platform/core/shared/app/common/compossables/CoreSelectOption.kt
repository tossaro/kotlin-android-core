@file:Suppress("UNUSED")

package multi.platform.core.shared.app.common.compossables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowDropUp
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.MenuDefaults
import androidx.compose.material3.MenuItemColors
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import org.jetbrains.compose.resources.DrawableResource
import org.jetbrains.compose.resources.painterResource

data class CoreOption(
    val text: String,
    val leadingIcon: DrawableResource? = null,
    val trailingIcon: DrawableResource? = null,
    val enabled: Boolean = true,
    val colors: MenuItemColors? = null,
    val contentPadding: PaddingValues = MenuDefaults.DropdownMenuItemContentPadding,
)

@OptIn(ExperimentalMaterial3Api::class)
@Suppress("kotlin:S107", "kotlin:S3776")
@Composable
fun CoreSelectOption(
    modifier: Modifier = Modifier,
    title: String?,
    titleTextStyle: TextStyle = MaterialTheme.typography.bodyLarge,
    textStyle: TextStyle = MaterialTheme.typography.bodyMedium,
    fieldModifier: Modifier = Modifier,
    options: List<CoreOption>,
    optionsModifier: Modifier = Modifier,
    optionsTextStyle: TextStyle = MaterialTheme.typography.bodyMedium,
    value: String?,
    hint: String = "Choose",
    onValueChange: (CoreOption) -> Unit,
) {
    val expanded = remember { mutableStateOf(false) }
    val selectedOptionText = remember { mutableStateOf(value) }

    Column(modifier = modifier) {
        if (title != null) {
            Text(
                text = title,
                style = titleTextStyle,
            )
        }
        ExposedDropdownMenuBox(
            modifier = Modifier.background(Color.Transparent),
            expanded = expanded.value,
            onExpandedChange = { expanded.value = !expanded.value },
        ) {
            Row(
                modifier = fieldModifier.menuAnchor(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                Text(
                    modifier = Modifier.menuAnchor(),
                    text = selectedOptionText.value ?: hint,
                    style = textStyle,

                )
                Icon(
                    if (expanded.value) Icons.Filled.ArrowDropUp else Icons.Filled.ArrowDropDown,
                    "contentDescription",
                )
            }
            ExposedDropdownMenu(
                modifier = optionsModifier,
                expanded = expanded.value,
                onDismissRequest = { expanded.value = false },
            ) {
                options.forEach { option ->
                    DropdownMenuItem(
                        text = { Text(text = option.text, style = optionsTextStyle) },
                        onClick = {
                            selectedOptionText.value = option.text
                            expanded.value = false
                            onValueChange(option)
                        },
                        leadingIcon = @Composable {
                            if (option.leadingIcon != null) {
                                Image(
                                    painterResource(option.leadingIcon),
                                    option.text,
                                )
                            }
                        },
                        trailingIcon = @Composable {
                            if (option.trailingIcon != null) {
                                Image(
                                    painterResource(option.trailingIcon),
                                    option.text,
                                )
                            }
                        },
                        enabled = option.enabled,
                        colors = option.colors ?: MenuDefaults.itemColors(),
                        contentPadding = option.contentPadding,
                    )
                }
            }
        }
    }
}
