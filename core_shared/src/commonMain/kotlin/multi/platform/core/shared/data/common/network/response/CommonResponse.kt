@file:Suppress("UNUSED")

package multi.platform.core.shared.data.common.network.response

import kotlinx.serialization.Serializable

@Serializable
data class CommonResponse<D>(
    var data: D? = null,
    var results: D? = null,
)
