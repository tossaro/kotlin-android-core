package multi.platform.core.shared.external.utilities.permission

import multi.platform.core.shared.external.enums.PermissionStateEnum

interface PermissionDelegate {
    fun getPermissionState(): PermissionStateEnum
    suspend fun providePermission()
    fun openSettingPage()
}
