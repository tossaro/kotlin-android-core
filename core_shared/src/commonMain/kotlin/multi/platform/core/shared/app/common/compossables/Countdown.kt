@file:Suppress("UNUSED")

package multi.platform.core.shared.app.common.compossables

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import io.ktor.util.date.getTimeMillis
import kotlinx.coroutines.delay

@Composable
fun Countdown(targetTime: Long, onComplete: () -> Unit, content: @Composable (remainingTime: Long) -> Unit) {
    var remainingTime by remember(targetTime) {
        mutableStateOf(targetTime - getTimeMillis())
    }
    content.invoke(remainingTime)
    LaunchedEffect(remainingTime) {
        val diff = remainingTime - (targetTime - getTimeMillis())
        if (remainingTime > 0) {
            delay(1_000L - diff)
            remainingTime = targetTime - getTimeMillis()
        } else {
            onComplete()
        }
    }
}
