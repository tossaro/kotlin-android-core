package multi.platform.core.shared.external.enums

enum class StateEnum {
    LOADING,
    SUCCESS,
    ERROR,
    EMPTY,
}
