package multi.platform.core.shared.external.utilities.media

data class VideoPlayerConfig(
    val urls: List<String>,
    val position: Int = 0,
    val repeatMode: Int = 0,
    val resizeMode: Int = 0,
    val showBufferMode: Int = 0,
    val useController: Boolean = true,
    val isAutoPlay: Boolean = true,
    val volume: Float? = null,
    val onPlaybackStateChanged: ((Int) -> Unit)? = null,
    val adEventListener: ((Any) -> Unit)? = null,
)
