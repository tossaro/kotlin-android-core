package multi.platform.core.shared.app.common.compossables

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil3.compose.AsyncImage
import multi.platform.core.shared.external.extensions.px

@Composable
fun UserAvatarView(
    modifier: Modifier = Modifier,
    size: Dp = 100.dp,
    profilePictUrl: String,
    borderColor: Color,
) {
    val stroke = PathEffect.dashPathEffect(floatArrayOf(10f, 10f), 0f)
    Box(
        modifier
            .size(size)
            .drawBehind {
                drawCircle(color = borderColor, style = Stroke(width = 4f, pathEffect = stroke))
            }
            .clip(CircleShape),
    ) {
        AsyncImage(
            modifier = Modifier.size(size - (size.px * 0.05).dp).align(Alignment.Center).clip(CircleShape),
            model = profilePictUrl,
            contentDescription = null,
            contentScale = ContentScale.Crop,
        )
    }
}
