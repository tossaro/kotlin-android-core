package multi.platform.core.shared.external.utilities

interface Persistent {
    fun putInt(key: String, value: Int)
    fun getInt(key: String, default: Int): Int
    fun putString(key: String, value: String)
    fun getString(key: String, default: String?): String?
    fun putBoolean(key: String, value: Boolean)
    fun getBoolean(key: String, default: Boolean): Boolean
    fun remove(key: String)
}
