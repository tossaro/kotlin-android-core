@file:Suppress("UNUSED", "UNCHECKED_CAST")

package multi.platform.core.shared.app.common

import io.ktor.client.plugins.ResponseException
import io.ktor.client.plugins.ServerResponseException
import io.ktor.utils.io.errors.IOException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import multi.platform.core.shared.data.common.network.response.CommonResponse
import multi.platform.core.shared.domain.common.usecase.CoreUseCase

abstract class ListViewModel<ITEM, RESP, GET : CoreUseCase, READ : CoreUseCase, SET : CoreUseCase>(
    private val getUseCase: GET,
    private val readUseCase: READ,
    private val setUseCase: SET,
) :
    CoreViewModel() {
    var items = MutableStateFlow<MutableList<ITEM>?>(null)
    var order: String? = null
    var search: Any? = null
    var limit = 0
    var page = 0

    open fun load(isFromNetworkOpt: Boolean = isFromNetwork) {
        if (isFromNetworkOpt) {
            getFromNetwork()
        } else {
            getFromLocal()
        }
    }

    open fun getFromLocal() {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            val results = readUseCase.call(
                limit * page,
                limit,
                search,
                order,
            ) as MutableList<ITEM>
            if (results.isEmpty()) {
                getFromNetwork()
            } else {
                scope.launch {
                    items.value = results
                    loadingIndicator.value = false
                }
            }
        }
    }

    open fun getFromNetwork() {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            scope.launch { loadingIndicator.value = true }
            try {
                val response = getUseCase.call(
                    limit,
                    page + 1,
                    search,
                    order,
                ) as RESP
                val temps = mutableListOf<ITEM>()
                when (response) {
                    is CommonResponse<*> -> {
                        response.results?.let {
                            (it as List<ITEM>).forEachIndexed { key, item ->
                                temps.add(modifyItemResponse(key, item))
                            }
                        }
                        response.data?.let {
                            (it as List<ITEM>).forEachIndexed { key, item ->
                                temps.add(modifyItemResponse(key, item))
                            }
                        }
                    }
                    is List<*> -> {
                        response.forEachIndexed { key, item ->
                            temps.add(modifyItemResponse(key, item))
                        }
                    }
                }
                saveToLocal(temps)
                scope.launch { loadingIndicator.value = false }
            } catch (e: Exception) {
                e.printStackTrace()
                scope.launch {
                    loadingIndicator.value = false
                    when (e) {
                        is ServerResponseException, is IOException -> onException.value = e
                        is ResponseException -> errorResponse.value = e.response
                        else -> errorMessage.value = e.message
                    }
                }
            }
        }
    }

    open suspend fun saveToLocal(newItems: MutableList<ITEM>?) {
        setUseCase.call(newItems)
        scope.launch { items.value = newItems }
    }
    open fun modifyItemResponse(key: Int, item: Any?) = item as ITEM
}
