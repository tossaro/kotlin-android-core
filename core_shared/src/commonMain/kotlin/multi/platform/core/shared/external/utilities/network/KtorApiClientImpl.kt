@file:Suppress("UNUSED")

package multi.platform.core.shared.external.utilities.network

import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.plugins.DefaultRequest
import io.ktor.client.plugins.HttpRequestRetry
import io.ktor.client.plugins.HttpResponseValidator
import io.ktor.client.plugins.RedirectResponseException
import io.ktor.client.plugins.ResponseException
import io.ktor.client.plugins.ServerResponseException
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.DEFAULT
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.bearerAuth
import io.ktor.client.request.header
import io.ktor.client.request.headers
import io.ktor.http.HttpHeaders
import io.ktor.http.URLProtocol
import io.ktor.http.fullPath
import io.ktor.serialization.kotlinx.json.json
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import multi.platform.core.shared.Context
import multi.platform.core.shared.domain.common.usecase.RefreshTokenUseCase
import multi.platform.core.shared.external.CoreConfig
import multi.platform.core.shared.external.constants.CommonKey
import multi.platform.core.shared.external.utilities.Persistent
import multi.platform.core.shared.getLanguage
import multi.platform.core.shared.getPlatform

class KtorApiClientImpl(
    httpClientEngine: HttpClientEngine,
    override val context: Context?,
    override val refreshTokenUseCase: RefreshTokenUseCase?,
    override val json: Json,
    override val coreConfig: CoreConfig,
    override val serverProtocol: URLProtocol?,
    override val server: String?,
    override val persistent: Persistent,
    override val deviceId: String,
    override val version: String,
) : ApiClientProvider<HttpClient> {
    @OptIn(DelicateCoroutinesApi::class)
    override val client = HttpClient(httpClientEngine) {
        install(ContentNegotiation) {
            json(json)
        }
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.ALL
        }
        install(DefaultRequest) {
            if (serverProtocol != null || server != null) {
                url {
                    protocol = serverProtocol ?: URLProtocol.HTTPS
                    server?.let { host = it }
                }
            }
            coreConfig.headerDeviceId?.let { header(it, deviceId) }
            coreConfig.headerLanguage?.let { header(it, getLanguage(context)) }
            coreConfig.headerOs?.let { header(it, getPlatform().name) }
            coreConfig.headerVersion?.let { header(it, version) }
            coreConfig.headerChannel?.let { k ->
                coreConfig.apiChannel?.let { v -> header(k, v) }
            }
        }
        expectSuccess = true
        HttpResponseValidator {
            validateResponse { response ->
                val statusCode = response.status.value
                when (statusCode) {
                    in 300..399 -> throw RedirectResponseException(response, "Unhandled redirect")
                    in 400..499 -> throw ClientRequestException(response, "Client request invalid")
                    in 500..599 -> throw ServerResponseException(response, "Server error")
                }
                if (statusCode >= 600) {
                    throw ResponseException(response, "Bad response")
                }
            }
        }
        coreConfig.apiRefreshTokenPath?.let {
            install(HttpRequestRetry) {
                retryIf { httpRequest, httpResponse ->
                    httpResponse.status.value == 401 &&
                        !httpRequest.url.fullPath.contains(
                            coreConfig.apiAuthPath.toString(),
                            ignoreCase = true,
                        ) &&
                        !httpRequest.url.fullPath.contains(it, ignoreCase = true)
                }
                refreshTokenUseCase?.let { rt ->
                    modifyRequest { request ->
                        GlobalScope.launch {
                            try {
                                rt.call()
                                persistent.putString(
                                    CommonKey.ACCESS_TOKEN_KEY,
                                    rt.newToken,
                                )
                                persistent.putString(
                                    CommonKey.REFRESH_TOKEN_KEY,
                                    rt.newRefreshToken,
                                )
                                request.headers { remove(HttpHeaders.Authorization) }
                                request.bearerAuth(rt.newToken)
                            } catch (e: Exception) {
                                e.printStackTrace()
                                rt.onError(e)
                            }
                        }
                    }
                }
            }
        }
    }
}
