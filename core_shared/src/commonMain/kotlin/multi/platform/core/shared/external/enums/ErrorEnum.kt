@file:Suppress("UNUSED")

package multi.platform.core.shared.external.enums

enum class ErrorEnum {
    AccountLocked,
    AuthError,
    DataNotFound,
    TokenError,
    TooManyRetryOTPLogin,
    TooManyRetryOTPRegistration,
    TooManyRetryEmailVerification,
    TooManyChangeEmailVerification,
    RequestOTPErrorLogin,
    RequestOTPErrorRegistration,
}
