package multi.platform.core.shared.app.common.compossables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign

@Composable
fun UserInitialView(
    modifier: Modifier = Modifier,
    textStyle: TextStyle,
    fullName: String,
) {
    Box(
        modifier.background(textStyle.background, shape = CircleShape),
    ) {
        val split = fullName.split(" ")
        var initial = split[0].first().toString()
        if (split.size > 1) initial += split[1].first().toString()
        Text(
            modifier = Modifier.align(Alignment.Center),
            text = initial,
            textAlign = TextAlign.Center,
            style = textStyle,
        )
    }
}
