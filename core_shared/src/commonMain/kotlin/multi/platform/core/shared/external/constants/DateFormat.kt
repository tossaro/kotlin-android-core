@file:Suppress("UNUSED")

package multi.platform.core.shared.external.constants

object DateFormat {
    const val NORMAL_DATE_FORMAT = "dd-MM-yyyy"
    const val API_DATE_FORMAT = "yyyy-MM-dd"
}
