@file:Suppress("UNUSED")

package multi.platform.core.shared.external.extensions

fun String?.orDash(): String = this.takeIf { !it.isNullOrBlank() } ?: "-"

fun String.substringBetween(startDelimiter: String, endDelimiter: String): String {
    val string = this
    val startIndex = string.lastIndexOf(startDelimiter)
    val endIndex = string.indexOf(endDelimiter)
    return if (startIndex > -1) string.substring(startIndex, endIndex) else ""
}

fun String.toColorInt(): Int {
    if (this[0] == '#') {
        var color = substring(1).toLong(16)
        if (length == 7) {
            color = color or 0x00000000ff000000L
        } else {
            require(length == 9) {
                throw IllegalArgumentException("Unknown color")
            }
        }
        return color.toInt()
    }
    throw IllegalArgumentException("Unknown color")
}
