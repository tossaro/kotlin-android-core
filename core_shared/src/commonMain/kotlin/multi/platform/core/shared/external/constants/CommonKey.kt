@file:Suppress("UNUSED")

package multi.platform.core.shared.external.constants

object CommonKey {
    const val ACCESS_TOKEN_KEY = "ACCESS_TOKEN"
    const val REFRESH_TOKEN_KEY = "REFRESH_TOKEN"
    const val PHONE_KEY = "PHONE"
    const val FILTERED_KEY = "FILTERED"
    const val SELECT_DATE_KEY = "SELECT_DATE"
    const val ONBOARDING_KEY = "ONBOARDING"
    const val PAYMENT_KEY = "PAYMENT"
    const val RETRY_KEY = "RETRY"
    const val ERROR_KEY = "ERROR"
    const val DISPLAY_NAME_KEY = "DISPLAY_NAME"
    const val LOCAL_ID_KEY = "LOCAL_ID"
    const val AVATAR_KEY = "AVATAR"
}
