@file:Suppress("UNUSED")

package multi.platform.core.shared.external.utilities.permission

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import multi.platform.core.shared.external.enums.PermissionEnum
import multi.platform.core.shared.external.enums.PermissionStateEnum
import multi.platform.core.shared.external.extensions.getPermissionDelegate
import org.koin.core.component.KoinComponent

class PermissionsService : KoinComponent {
    fun checkPermission(permission: PermissionEnum): PermissionStateEnum {
        return try {
            return getPermissionDelegate(permission).getPermissionState()
        } catch (e: Exception) {
            println("Failed to check permission $permission")
            e.printStackTrace()
            PermissionStateEnum.NOT_DETERMINED
        }
    }

    fun checkPermissionFlow(permission: PermissionEnum): Flow<PermissionStateEnum> {
        return flow {
            while (true) {
                val permissionState = checkPermission(permission)
                emit(permissionState)
                delay(PERMISSION_CHECK_FLOW_FREQUENCY)
            }
        }
    }

    suspend fun providePermission(permission: PermissionEnum) {
        try {
            getPermissionDelegate(permission).providePermission()
        } catch (e: Exception) {
            println("Failed to request permission $permission")
            e.printStackTrace()
        }
    }
    fun openSettingPage(permission: PermissionEnum) {
        println("Open settings for permission $permission")
        try {
            getPermissionDelegate(permission).openSettingPage()
        } catch (e: Exception) {
            println("Failed to open settings for permission $permission")
            e.printStackTrace()
        }
    }

    companion object {
        const val PERMISSION_CHECK_FLOW_FREQUENCY = 1000L
    }
}
