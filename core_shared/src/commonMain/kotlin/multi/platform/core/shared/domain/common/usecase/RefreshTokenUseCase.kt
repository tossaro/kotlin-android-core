package multi.platform.core.shared.domain.common.usecase

interface RefreshTokenUseCase : CoreUseCase {
    /**
     * Override this function for get new token and refresh token.
     * Don't forget to store response value to newToken and newRefreshToken variable.
     * With stored response to mentioned variable, will be saved to encrypted local data.
     * Example: Invoke repository that handle refresh token api
     */
    override suspend fun call(vararg args: Any?)

    /**
     * Override this function for handling any error occurred.
     * Example: Remove saved token and refresh token
     */
    override suspend fun onError(e: Exception)

    /**
     * This variable should valued from succeed call().
     * Useful for saving to local data and retry current expired api call
     */
    var newToken: String

    /**
     * This variable should valued from succeed call().
     * Useful for saving to local data
     */
    var newRefreshToken: String
}
