@file:Suppress("UNUSED")

package multi.platform.core.shared.app.common.compossables

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsFocusedAsState
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldColors
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.takeOrElse
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp

enum class TextFieldType {
    Filled, Outlined
}

@Suppress("kotlin:S107")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CoreTextField(
    modifier: Modifier = Modifier,
    fieldModifier: Modifier = Modifier.fillMaxWidth(),
    title: String?,
    titleTextStyle: TextStyle = MaterialTheme.typography.bodyLarge,
    value: String?,
    onValueChange: (String) -> Unit,
    textFieldType: TextFieldType = TextFieldType.Filled,
    label: (@Composable () -> Unit)? = null,
    placeholder: (@Composable () -> Unit)? = null,
    maxLines: Int = 1,
    validation: (@Composable (String?) -> String?)? = null,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    colors: TextFieldColors = TextFieldDefaults.colors().copy(
        unfocusedContainerColor = Color.Transparent,
        focusedContainerColor = Color.Transparent,
        errorContainerColor = Color.Transparent,
    ),
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    textStyle: TextStyle = LocalTextStyle.current,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    leadingIcon: (@Composable () -> Unit)? = null,
    trailingIcon: (@Composable () -> Unit)? = null,
    prefix: @Composable (() -> Unit)? = null,
    suffix: @Composable (() -> Unit)? = null,
    supportingText: @Composable (() -> Unit)? = null,
    shape: Shape = TextFieldDefaults.shape,
    contentPadding: PaddingValues = PaddingValues(0.dp),
) {
    val error = validation?.invoke(value)
    val interactionSource = remember { MutableInteractionSource() }
    val textColor = textStyle.color.takeOrElse {
        val focused by interactionSource.collectIsFocusedAsState()
        val targetValue = when {
            !enabled -> TextFieldDefaults.colors().disabledTextColor
            error != null -> TextFieldDefaults.colors().errorTextColor
            focused -> TextFieldDefaults.colors().focusedTextColor
            else -> TextFieldDefaults.colors().unfocusedTextColor
        }
        rememberUpdatedState(targetValue).value
    }
    val mergedTextStyle = textStyle.merge(TextStyle(color = textColor))

    Column(modifier = modifier) {
        if (title != null) {
            Text(
                text = title,
                style = titleTextStyle.copy(color = textStyle.color),
            )
        }
        CompositionLocalProvider(LocalTextSelectionColors provides colors.textSelectionColors) {
            BasicTextField(
                value = value ?: "",
                onValueChange = onValueChange,
                modifier = fieldModifier,
                visualTransformation = visualTransformation,
                interactionSource = interactionSource,
                cursorBrush = SolidColor(rememberUpdatedState(if (error != null) TextFieldDefaults.colors().errorCursorColor else TextFieldDefaults.colors().cursorColor).value),
                enabled = enabled,
                readOnly = readOnly,
                maxLines = maxLines,
                keyboardOptions = keyboardOptions,
                keyboardActions = keyboardActions,
                singleLine = maxLines == 1,
                textStyle = mergedTextStyle,
            ) { innerTextField ->
                if (textFieldType == TextFieldType.Filled) {
                    TextFieldDefaults.DecorationBox(
                        value = value ?: "",
                        visualTransformation = visualTransformation,
                        innerTextField = innerTextField,
                        placeholder = placeholder,
                        label = label,
                        leadingIcon = leadingIcon,
                        trailingIcon = trailingIcon,
                        prefix = prefix,
                        suffix = suffix,
                        supportingText = supportingText,
                        shape = shape,
                        singleLine = maxLines == 1,
                        enabled = enabled,
                        isError = error != null,
                        interactionSource = interactionSource,
                        colors = colors,
                        contentPadding = contentPadding,
                    )
                } else if (textFieldType == TextFieldType.Outlined) {
                    OutlinedTextFieldDefaults.DecorationBox(
                        value = value ?: "",
                        visualTransformation = visualTransformation,
                        innerTextField = innerTextField,
                        placeholder = placeholder,
                        label = label,
                        leadingIcon = leadingIcon,
                        trailingIcon = trailingIcon,
                        prefix = prefix,
                        suffix = suffix,
                        supportingText = supportingText,
                        singleLine = maxLines == 1,
                        enabled = enabled,
                        isError = error != null,
                        interactionSource = interactionSource,
                        colors = colors,
                        contentPadding = contentPadding,
                        container = {
                            OutlinedTextFieldDefaults.ContainerBox(
                                enabled,
                                error != null,
                                interactionSource,
                                colors,
                                shape,
                            )
                        },
                    )
                }
            }
        }
        error?.let {
            Text(
                text = it,
                style = MaterialTheme.typography.bodyMedium,
                color = MaterialTheme.colorScheme.error,
                modifier = Modifier.fillMaxWidth().padding(top = 4.dp),
            )
        }
    }
}
