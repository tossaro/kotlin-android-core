package multi.platform.core.shared.external.utilities.media

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
expect fun VideoPlayer(modifier: Modifier, videoPlayerConfig: VideoPlayerConfig)
