@file:Suppress("UNUSED", "kotlin:S6305", "kotlin:S6305")

package multi.platform.core.shared.app.common

import io.ktor.client.statement.HttpResponse
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.MutableStateFlow
import multi.platform.core.shared.external.utilities.Validation

actual abstract class CoreViewModel {
    actual var errorEmptyField: String? = null
    actual var errorMinChar: String? = null
    actual var errorPhoneFormat: String? = null
    actual var errorEmailFormat: String? = null
    actual var errorPasswordFormat: String? = null
    actual var errorConfirm: String? = null
    actual var accessToken: String? = null
    actual val isEmpty = MutableStateFlow(false)
    actual var isFromNetwork = true
    actual var useAsyncNetworkCall = true

    actual val loadingIndicator = MutableStateFlow<Boolean?>(false)
    actual val toastMessage = MutableStateFlow<String?>(null)
    actual val successMessage = MutableStateFlow<String?>(null)
    actual val errorMessage = MutableStateFlow<String?>(null)
    actual val errorResponse = MutableStateFlow<HttpResponse?>(null)
    actual val forceSignout = MutableStateFlow(false)
    actual val onException = MutableStateFlow<Exception?>(null)
    actual val scope = MainScope()

    actual fun validateBlank(
        field: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean {
        val check = Validation.notBlank(field.value) == false
        error.value = if (check) errorEmptyField else null
        return check
    }

    actual fun validateMinChar(
        min: Int,
        field: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean {
        val check = Validation.minCharacter(min, field.value) == false
        error.value = if (check) errorMinChar else null
        return check
    }

    actual fun validatePhoneFormat(
        field: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean {
        val check = Validation.phoneFormat(field.value) == false
        error.value = if (check) errorPhoneFormat else null
        return check
    }

    actual fun validateEmailFormat(
        field: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean {
        val check = Validation.emailFormat(field.value) == false
        error.value = if (check) errorEmailFormat else null
        return check
    }

    actual fun validatePasswordFormat(
        field: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean {
        val check = Validation.passwordFormat(field.value) == false
        error.value = if (check) errorPasswordFormat else null
        return check
    }

    actual fun validateConfirm(
        field1: MutableStateFlow<String?>,
        field2: MutableStateFlow<String?>,
        error: MutableStateFlow<String?>,
    ): Boolean {
        val check = field1.value != field2.value
        error.value = if (check) errorConfirm else null
        return check
    }
}
