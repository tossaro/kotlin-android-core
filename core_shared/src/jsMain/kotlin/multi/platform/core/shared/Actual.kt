@file:Suppress("UNUSED", "kotlin:S1172")

package multi.platform.core.shared

import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.js.Js
import org.koin.dsl.module
import kotlin.js.Date
import kotlin.math.pow
import kotlin.math.roundToInt

actual typealias Context = NotImplementedError

class DesktopPlatform : Platform {
    override val name: String = "Web with Kotlin/Wasm"
    override val apiEngine: HttpClientEngine = Js.create()
}

actual fun getPlatform(): Platform = DesktopPlatform()
actual fun getLanguage(context: Context?) = "ID"

actual interface Parcelable

actual fun formatDate(dateString: String, fromFormat: String, toFormat: String): String {
    val date = if (dateString.isNotEmpty()) Date(dateString) else Date()
    return date.toLocaleString("id", dateLocaleOptions { formatMatcher = toFormat })
}

actual fun platformModule() = module {
    single { Js.create() }
}

actual class DecimalFormat {
    actual fun format(double: Double, maximumFractionDigits: Int): String {
        val factor = 10.0.pow(maximumFractionDigits)
        return ((double * factor).roundToInt() / factor).toString()
    }
}
