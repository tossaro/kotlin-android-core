// @file:Suppress("UNUSED")
//
// package multi.platform.core.shared
//
// import io.ktor.client.engine.HttpClientEngine
// import io.ktor.client.engine.HttpClientEngineConfig
// import io.ktor.client.request.HttpRequestData
// import io.ktor.client.request.HttpResponseData
// import io.ktor.util.InternalAPI
// import kotlinx.coroutines.CoroutineDispatcher
// import org.koin.dsl.module
// import kotlin.coroutines.CoroutineContext
//
// actual class Context
//
// class CommonPlatform : Platform {
//    override val name: String = "Common"
//    override val apiEngine: HttpClientEngine = object : HttpClientEngine {
//        override val config: HttpClientEngineConfig
//            get() = throw UnsupportedOperationException("Not yet implemented")
//        override val dispatcher: CoroutineDispatcher
//            get() = throw UnsupportedOperationException("Not yet implemented")
//
//        @InternalAPI
//        override suspend fun execute(data: HttpRequestData): HttpResponseData {
//            throw UnsupportedOperationException("Not yet implemented")
//        }
//
//        override fun close() {
//            throw UnsupportedOperationException("Not yet implemented")
//        }
//
//        override val coroutineContext: CoroutineContext
//            get() = throw UnsupportedOperationException("Not yet implemented")
//    }
// }
//
// actual fun getPlatform(): Platform = CommonPlatform()
// actual fun getLanguage(context: Context?): String {
//    return "EN"
// }
//
// actual interface Parcelable
//
// @Suppress("SimpleDateFormat")
// actual fun formatDate(dateString: String, fromFormat: String, toFormat: String): String {
//    return "result"
// }
//
// actual fun platformModule() = module {}
//
// actual class DecimalFormat {
//    actual fun format(double: Double, maximumFractionDigits: Int): String {
//        return "result"
//    }
// }
