package multi.platform.core.shared.domain.common.usecase

import io.mockk.clearAllMocks
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import kotlin.test.BeforeTest
import kotlin.test.DefaultAsserter.fail
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class CoreUseCaseTest {
    private lateinit var useCase: CoreUseCase

    @BeforeTest
    fun setup() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        clearAllMocks()
        useCase = object : CoreUseCase {
            override suspend fun call(vararg args: Any?) = "Expected Result"
        }
    }

    @Test
    fun `call should return expected result`() = runTest {
        // Act
        val result = useCase.call() as String

        // Assert
        assertEquals("Expected Result", result)
    }

    @Test
    fun `onError should throw UnsupportedOperationException`() = runTest {
        try {
            // Act
            useCase.onError(Exception())

            // Assert
            fail("Expected UnsupportedOperationException was not thrown")
        } catch (e: UnsupportedOperationException) {
            // Exception was thrown as expected
        }
    }
}
