package multi.platform.core.shared.app.common

import io.mockk.clearAllMocks
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class CoreViewModelTest {
    private lateinit var viewModel: CoreViewModel

    @BeforeTest
    fun setup() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        clearAllMocks()
        viewModel = object : CoreViewModel() {}
        viewModel.useAsyncNetworkCall = false
    }

    @Test
    fun `enable network call by default`() = runTest {
        val result = true
        assertEquals(result, viewModel.isFromNetwork)
    }
}
