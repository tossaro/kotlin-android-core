package multi.platform.core.shared.domain.common.usecase

import io.mockk.clearAllMocks
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class RefreshTokenUseCaseTest {
    private lateinit var useCase: RefreshTokenUseCase

    @BeforeTest
    fun setup() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        clearAllMocks()
        useCase = object : RefreshTokenUseCase {
            override suspend fun call(vararg args: Any?) {
                newToken = "newTokenValue"
                newRefreshToken = "newRefreshTokenValue"
            }
            override suspend fun onError(e: Exception) {
                newToken = ""
                newRefreshToken = ""
            }
            override var newToken: String = "oldTokenValue"
            override var newRefreshToken: String = "oldRefreshTokenValue"
        }
    }

    @Test
    fun `call should store new token and refresh token`() = runTest {
        // Act
        useCase.call()

        // Assert
        assertEquals("newTokenValue", useCase.newToken)
        assertEquals("newRefreshTokenValue", useCase.newRefreshToken)
    }

    @Test
    fun `onError should handle the error`() = runTest {
        // Act
        useCase.onError(Exception())

        // Assert
        assertEquals("", useCase.newToken)
        assertEquals("", useCase.newRefreshToken)
    }
}
