package multi.platform.core.shared.app.common

import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import multi.platform.core.shared.domain.common.usecase.CoreUseCase
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@OptIn(ExperimentalCoroutinesApi::class)
class ListViewModelTest {
    private lateinit var viewModel: ListViewModel<String, String, CoreUseCase, CoreUseCase, CoreUseCase>
    private val getUseCase = mockk<CoreUseCase>()
    private val readUseCase = mockk<CoreUseCase>()
    private val setUseCase = mockk<CoreUseCase>()

    private val page = 0
    private val limit = 1
    private val search = null
    private val order = null

    @BeforeTest
    fun setup() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        clearAllMocks()
        viewModel = object : ListViewModel<String, String, CoreUseCase, CoreUseCase, CoreUseCase>(
            getUseCase,
            readUseCase,
            setUseCase,
        ) {}
        viewModel.useAsyncNetworkCall = false
        viewModel.page = page
        viewModel.limit = limit
        viewModel.search = search
        viewModel.order = order
    }

    @Test
    fun `test getFromNetwork should return item from getUseCase`() = runTest {
        // Arrange
        val Items = mutableListOf("item")
        coEvery { getUseCase.call(limit, page + 1, search, order) } returns Items
        coEvery { setUseCase.call(Items) } returns true

        // Act
        viewModel.getFromNetwork()
        advanceUntilIdle()

        // Assert
        assertNotNull(viewModel.items.value)
        assertEquals(Items, viewModel.items.value)
    }

    @Test
    fun `test getFromLocal should return item from readUseCase`() = runTest {
        // Arrange
        val Items = mutableListOf("item")
        coEvery { readUseCase.call(limit * page, limit, search, order) } returns Items

        // Act
        viewModel.getFromLocal()
        advanceUntilIdle()

        // Assert
        assertNotNull(viewModel.items.value)
        assertEquals(Items, viewModel.items.value)
    }

    @Test
    fun `test saveToLocal should update item`() = runTest {
        // Arrange
        viewModel.items.value = mutableListOf()
        val newItems = mutableListOf("item")
        coEvery { setUseCase.call(newItems) } returns true

        // Act
        viewModel.saveToLocal(newItems)
        advanceUntilIdle()

        // Assert
        assertEquals(newItems, viewModel.items.value)
    }
}
