package multi.platform.core.shared.app.common

import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import multi.platform.core.shared.domain.common.usecase.CoreUseCase
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@OptIn(ExperimentalCoroutinesApi::class)
class ItemViewModelTest {
    private val getUseCase = mockk<CoreUseCase>()
    private val readUseCase = mockk<CoreUseCase>()
    private val saveUseCase = mockk<CoreUseCase>()
    private val submitUseCase = mockk<CoreUseCase>()
    private val deleteUseCase = mockk<CoreUseCase>()
    private val removeUseCase = mockk<CoreUseCase>()
    private lateinit var viewModel: ItemViewModel<String, CoreUseCase, CoreUseCase, CoreUseCase, CoreUseCase, CoreUseCase, CoreUseCase>
    private val query = "test"

    @BeforeTest
    fun setup() = runTest {
        Dispatchers.setMain(StandardTestDispatcher(testScheduler))
        clearAllMocks()
        viewModel = object :
            ItemViewModel<String, CoreUseCase, CoreUseCase, CoreUseCase, CoreUseCase, CoreUseCase, CoreUseCase>(
                getUseCase,
                readUseCase,
                saveUseCase,
                submitUseCase,
                deleteUseCase,
                removeUseCase,
            ) {}
        viewModel.useAsyncNetworkCall = false
    }

    @Test
    fun `test getFromNetwork should return item from getUseCase`() = runTest {
        // Arrange
        val item = "item"
        coEvery { getUseCase.call(query) } returns item
        coEvery { saveUseCase.call(item) } returns item

        // Act
        viewModel.getFromNetwork(query)
        advanceUntilIdle()

        // Assert
        assertNotNull(viewModel.item.value)
        assertEquals(item, viewModel.item.value)
    }

    @Test
    fun `test getFromLocal should return item from readUseCase`() = runTest {
        // Arrange
        val item = "item"
        coEvery { readUseCase.call(query) } returns item

        // Act
        viewModel.getFromLocal(query)
        advanceUntilIdle()

        // Assert
        assertNotNull(viewModel.item.value)
        assertEquals(item, viewModel.item.value)
    }

    @Test
    fun `test saveToLocal should update item`() = runTest {
        // Arrange
        viewModel.item.value = "item"
        val newItem = "newItem"
        coEvery { saveUseCase.call(newItem) } returns newItem

        // Act
        viewModel.saveToLocal(newItem)
        advanceUntilIdle()

        // Assert
        assertEquals(newItem, viewModel.item.value)
    }

    @Test
    fun `test modifyItemResponse should return item`() = runTest {
        // Arrange
        val item = "item"

        // Act
        val modified = viewModel.modifyItemResponse(item)

        // Assert
        assertEquals(item, modified)
    }

    @Test
    fun `test deleteOnNetwork should return true from deleteUseCase`() = runTest {
        // Arrange
        val result = true
        coEvery { deleteUseCase.call(query) } returns result
        coEvery { removeUseCase.call(query) } returns result

        // Act
        viewModel.deleteOnNetwork(query)
        advanceUntilIdle()

        // Assert
        assertNotNull(viewModel.onDeleted.value)
        assertEquals(result, viewModel.onDeleted.value)
    }

    @Test
    fun `test removeOnLocal should return onDeleted true`() = runTest {
        // Arrange
        viewModel.onDeleted.value = false
        val result = true
        coEvery { removeUseCase.call(query) } returns result

        // Act
        viewModel.removeOnLocal(query)
        advanceUntilIdle()

        // Assert
        assertEquals(result, viewModel.onDeleted.value)
    }

    @Test
    fun `test validateBeforeSubmit should return true`() = runTest {
        // Arrange
        val newItem = "newItem"
        val result = true

        // Act
        val validated = viewModel.validateBeforeSubmit(newItem)

        // Assert
        assertEquals(result, validated)
    }

    @Test
    fun `test submitToNetwork should update item`() = runTest {
        // Arrange
        viewModel.item.value = "item"
        val newItem = "newItem"
        coEvery { submitUseCase.call(newItem) } returns newItem
        coEvery { saveUseCase.call(newItem) } returns newItem

        // Act
        viewModel.submitToNetwork(newItem)
        advanceUntilIdle()

        // Assert
        assertEquals(newItem, viewModel.item.value)
    }
}
