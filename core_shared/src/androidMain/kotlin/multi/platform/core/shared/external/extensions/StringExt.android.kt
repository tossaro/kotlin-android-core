@file:Suppress("UNUSED")

package multi.platform.core.shared.external.extensions

import android.text.Html
import android.text.Spanned
import java.security.MessageDigest
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.TimeZone
import java.util.regex.Pattern

fun String.buildUtcDateFormat() = SimpleDateFormat(this, Locale.getDefault()).apply {
    timeZone = TimeZone.getTimeZone("UTC")
}

fun String.buildSimpleDateFormat() = SimpleDateFormat(this, Locale.getDefault())

fun String.getAlphaNumericDashAfterColon(): String {
    if (this.isNotBlank()) {
        val pattern = Pattern.compile(": (\\w+)")
        val matcher = pattern.matcher(this)
        if (matcher.find()) {
            val result = matcher.group()
            return result.substring(result.lastIndexOf(' ') + 1)
        }
    }
    return ""
}

fun String.toCurrency(symbol: String): String {
    var result = replace(symbol, "")
    if (result.isNotEmpty()) {
        try {
            result = result.replace(",", "").replace(".", "")
            val strLong = result.toLong()
            val formatter = NumberFormat.getInstance(Locale.US) as DecimalFormat
            formatter.applyPattern("#,###,###,###")
            var formatted = formatter.format(strLong)
            if (symbol == "Rp " || symbol == "IDR ") formatted = formatted.replace(",", ".")
            result = symbol + formatted
        } catch (e: java.lang.NumberFormatException) {
            e.printStackTrace()
            result = this
        }
    }
    return result
}

fun String.toHtml(): Spanned? {
    if (this.isNotEmpty()) {
        return Html.fromHtml(
            this,
            Html.FROM_HTML_MODE_LEGACY,
        )
    }
    return null
}

fun String.toMD5(): String {
    val bytes = MessageDigest.getInstance("MD5").digest(this.toByteArray())
    return bytes.joinToString("") { "%02x".format(it) }
}
