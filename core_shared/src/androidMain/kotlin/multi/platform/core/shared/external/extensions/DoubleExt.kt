@file:Suppress("UNUSED")

package multi.platform.core.shared.external.extensions

import java.text.NumberFormat

fun Double.roundAsString(maxFractionDigits: Int = 0): String {
    val nf = NumberFormat.getInstance()
    nf.maximumFractionDigits = maxFractionDigits
    return nf.format(this)
}

fun Double.toCurrency(symbol: String, maxFractionDigits: Int = 0) =
    this.roundAsString(maxFractionDigits).toCurrency(symbol)
