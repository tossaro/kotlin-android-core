package multi.platform.core.shared.app

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import androidx.annotation.CallSuper
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.http.URLProtocol
import multi.platform.core.shared.BuildConfig
import multi.platform.core.shared.createJson
import multi.platform.core.shared.external.utilities.EncryptedPersistentImpl
import multi.platform.core.shared.external.utilities.LocaleUtil
import multi.platform.core.shared.external.utilities.Persistent
import multi.platform.core.shared.external.utilities.network.ApiClientProvider
import multi.platform.core.shared.external.utilities.network.KtorApiClientImpl
import multi.platform.core.shared.external.utilities.network.RetrofitApiClientImpl
import multi.platform.core.shared.getPlatform
import multi.platform.core.shared.platformModule
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.EmptyLogger
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.KoinAppDeclaration
import org.koin.dsl.module
import retrofit2.Retrofit
import timber.log.Timber
import java.security.KeyStore
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.util.concurrent.TimeUnit
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

abstract class CoreApplication : Application() {

    companion object {
        lateinit var appContext: Context
    }

    enum class NetworkClientType { KTOR, RETROFIT }

    /**
     * Abstract function must override shared preferences name
     */
    abstract fun sharedPrefsName(): String

    /**
     * Open function to override server protocol
     */
    open fun apiProtocol(): URLProtocol? = null

    /**
     * Abstract function must override server host
     */
    open fun apiHost(): String? = null

    /**
     * Abstract function must override app version
     */
    abstract fun appVersion(): String

    /**
     * Abstract function must override device id
     */
    abstract fun deviceId(): String

    /**
     * Open function for network client used
     * Default: NetworkClientType.KTOR
     */
    open fun networkClientType() = NetworkClientType.KTOR

    /**
     * Abstract variable must override KOIN App to add any modules
     */
    abstract val koinApp: KoinAppDeclaration

    /**
     * Open function for trust manager
     */
    @Suppress("kotlin:S1186", "kotlin:S4830", "CustomX509TrustManager", "TrustAllX509TrustManager")
    open fun provideX509TrustManager() = object : X509TrustManager {
        @Throws(CertificateException::class)
        override fun checkClientTrusted(
            chain: Array<java.security.cert.X509Certificate>,
            authType: String,
        ) {
        }

        @Throws(CertificateException::class)
        override fun checkServerTrusted(
            chain: Array<java.security.cert.X509Certificate>,
            authType: String,
        ) {
        }

        override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
            return arrayOf()
        }
    }

    /**
     * Open function for okhttp client builder
     */
    @Suppress("kotlin:S5527")
    open fun provideOkHttpClient(x509TrustManager: X509TrustManager): OkHttpClient.Builder {
        val client = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)

        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        client.addInterceptor(httpLoggingInterceptor)

        val trustAllCerts = arrayOf<TrustManager>(x509TrustManager)
        try {
            val keyStore = KeyStore.getInstance(KeyStore.getDefaultType())
            keyStore.load(null, null)

            val sslContext = SSLContext.getInstance("TLSv1.2")

            val trustManagerFactory =
                TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(keyStore)
            val keyManagerFactory =
                KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm())
            keyManagerFactory.init(keyStore, "keystore_pass".toCharArray())
            sslContext.init(null, trustAllCerts, SecureRandom())
            client.sslSocketFactory(sslContext.socketFactory, x509TrustManager)
                .hostnameVerifier { _, _ -> true }
        } catch (e: Exception) {
            e.printStackTrace()
            Timber.e(e)
        }

        return client
    }

    /**
     * Open function for add initialize application
     * Triggered by onCreate
     * Super: Timber
     */
    @CallSuper
    protected open fun onLaunch() {
        Timber.plant(Timber.DebugTree())
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleUtil.onAttach(this)
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        onLaunch()
        startKoin {
            if (BuildConfig.DEBUG) androidLogger() else EmptyLogger()
            androidContext(applicationContext)
            modules(
                module {
                    single<HttpClientEngine> { getPlatform().apiEngine }
                    single<Persistent> { EncryptedPersistentImpl(get(), sharedPrefsName()) }
                    singleOf(::createJson)
                    singleOf(::provideX509TrustManager)
                    singleOf(::provideOkHttpClient)
                    when (networkClientType()) {
                        NetworkClientType.KTOR -> single<ApiClientProvider<HttpClient>> {
                            KtorApiClientImpl(
                                get(),
                                get(),
                                getOrNull(),
                                get(),
                                get(),
                                apiProtocol(),
                                apiHost(),
                                get(),
                                deviceId(),
                                appVersion(),
                            )
                        }

                        NetworkClientType.RETROFIT -> single<ApiClientProvider<Retrofit>> {
                            RetrofitApiClientImpl(
                                get(),
                                get(),
                                getOrNull(),
                                get(),
                                get(),
                                apiProtocol(),
                                apiHost(),
                                get(),
                                deviceId(),
                                appVersion(),
                            )
                        }
                    }
                },
                platformModule(),
            )
            koinApp()
        }
    }
}
