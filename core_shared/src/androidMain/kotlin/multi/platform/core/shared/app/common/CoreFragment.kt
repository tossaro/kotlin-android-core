package multi.platform.core.shared.app.common

import android.os.Build
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.fragment.app.Fragment
import org.koin.core.component.KoinComponent

open class CoreFragment : Fragment(), KoinComponent {
    /**
     * Open function for override navigation bar translucent
     * Default: false
     */
    protected open fun isNavBarTranslucent() = false

    /**
     * Open function for override full screen
     * Default: false
     */
    protected open fun isFullScreen() = false

    /**
     * Open function for override resize capability
     * Default: true
     */
    protected open fun shouldResize() = true

    /**
     * Open function for override status bar color
     * Default: null
     */
    protected open fun statusBarColor(): Int? = null

    /**
     * Open function for override status bar color
     * Default: null
     */
    protected open fun navBarColor(): Int? = null

    @Suppress("DEPRECATION")
    protected open fun setNavBarColor(window: Window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && navBarColor() != null) {
            window.navigationBarColor = ContextCompat.getColor(
                requireContext(),
                if (isNavBarTranslucent()) android.R.color.transparent else navBarColor()!!,
            )
        } else {
            if (isNavBarTranslucent()) {
                window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
            } else window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        }
    }

    @Suppress("DEPRECATION")
    protected open fun setResize(window: Window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(shouldResize())
        } else {
            window.setSoftInputMode(if (shouldResize().not()) WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE else WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        }
    }

    @Suppress("ClickableViewAccessibility")
    override fun onResume() {
        super.onResume()
        activity?.window?.run {
            if (isFullScreen()) {
                WindowInsetsControllerCompat(this, decorView).apply {
                    hide(WindowInsetsCompat.Type.statusBars())
                    hide(WindowInsetsCompat.Type.systemBars())
                    systemBarsBehavior =
                        WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                }
            }
            setResize(this)
            statusBarColor()?.let {
                statusBarColor = ContextCompat.getColor(requireContext(), it)
            }
            setNavBarColor(this)
        }
    }
}
