package multi.platform.core.shared.external.utilities.permission

import android.Manifest
import android.app.Activity
import android.content.Context
import android.os.Build
import multi.platform.core.shared.external.enums.PermissionEnum
import multi.platform.core.shared.external.enums.PermissionStateEnum
import multi.platform.core.shared.external.extensions.checkPermissions
import multi.platform.core.shared.external.extensions.openAppSettingsPage
import multi.platform.core.shared.external.extensions.providePermissions

internal class LocationForegroundPermissionDelegate(
    private val context: Context,
    private val activity: Lazy<Activity>,
) : PermissionDelegate {
    override fun getPermissionState(): PermissionStateEnum {
        return context.checkPermissions(activity, fineLocationPermissions)
    }

    override suspend fun providePermission() {
        activity.value.providePermissions(fineLocationPermissions) {
            throw Exception(
                it.localizedMessage ?: "Failed to request foreground location permission",
            )
        }
    }

    override fun openSettingPage() {
        context.openAppSettingsPage(PermissionEnum.LOCATION_FOREGROUND)
    }
}

internal val fineLocationPermissions: List<String> =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        listOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
        )
    } else {
        listOf(Manifest.permission.ACCESS_FINE_LOCATION)
    }
