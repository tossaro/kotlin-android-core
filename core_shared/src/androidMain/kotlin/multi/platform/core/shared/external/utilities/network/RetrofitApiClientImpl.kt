@file:Suppress("kotlin:S1186")

package multi.platform.core.shared.external.utilities.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import io.ktor.http.URLProtocol
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import multi.platform.core.shared.Context
import multi.platform.core.shared.domain.common.usecase.RefreshTokenUseCase
import multi.platform.core.shared.external.CoreConfig
import multi.platform.core.shared.external.constants.CommonKey
import multi.platform.core.shared.external.utilities.Persistent
import multi.platform.core.shared.getLanguage
import multi.platform.core.shared.getPlatform
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit

class RetrofitApiClientImpl(
    private val okHttpClientBuilder: OkHttpClient.Builder,
    override val context: Context?,
    override val refreshTokenUseCase: RefreshTokenUseCase?,
    override val json: Json,
    override val coreConfig: CoreConfig,
    override val serverProtocol: URLProtocol?,
    override val server: String?,
    override val persistent: Persistent,
    override val deviceId: String,
    override val version: String,
) : ApiClientProvider<Retrofit> {
    override val client: Retrofit
        get() {
            okHttpClientBuilder.addInterceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder().also {
                    it.method(original.method, original.body)
                    coreConfig.headerDeviceId?.let { h -> it.header(h, deviceId) }
                    coreConfig.headerLanguage?.let { h -> it.header(h, getLanguage(context)) }
                    coreConfig.headerOs?.let { h -> it.header(h, getPlatform().name) }
                    coreConfig.headerVersion?.let { h -> it.header(h, version) }
                    coreConfig.headerChannel?.let { k ->
                        coreConfig.apiChannel?.let { v -> it.header(k, v) }
                    }
                }
                var response = chain.proceed(request.build())
                coreConfig.apiRefreshTokenPath?.let {
                    if (response.code == 401 &&
                        !original.url.encodedPath.contains(it, ignoreCase = true) &&
                        !original.url.encodedPath.contains(
                            coreConfig.apiAuthPath.toString(),
                            ignoreCase = true,
                        )
                    ) {
                        refreshTokenUseCase?.let { rt ->
                            runBlocking {
                                try {
                                    rt.call()
                                    persistent.putString(
                                        CommonKey.ACCESS_TOKEN_KEY,
                                        rt.newToken,
                                    )
                                    persistent.putString(
                                        CommonKey.REFRESH_TOKEN_KEY,
                                        rt.newRefreshToken,
                                    )
                                    request.removeHeader("Authorization")
                                    request.addHeader("Authorization", "Bearer ${rt.newToken}")
                                    response.close()
                                    response = chain.proceed(request.build())
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                    rt.onError(e)
                                }
                            }
                        }
                    }
                }
                response
            }
            val retrofit = Retrofit.Builder()
                .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
                .client(okHttpClientBuilder.build())
            if (serverProtocol != null && server != null) {
                retrofit.baseUrl(serverProtocol.name + server)
            }
            return retrofit.build()
        }
}
