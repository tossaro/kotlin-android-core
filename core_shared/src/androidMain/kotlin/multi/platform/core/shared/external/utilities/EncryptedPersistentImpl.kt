package multi.platform.core.shared.external.utilities

import android.content.Context
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey

class EncryptedPersistentImpl(
    context: Context,
    key: String,
) : Persistent {
    val masterKey = MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
        .build()
    val sharedPref = EncryptedSharedPreferences.create(
        context,
        key,
        masterKey,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM,
    )

    override fun putInt(key: String, value: Int) {
        sharedPref.edit().putInt(key, value).apply()
    }

    override fun getInt(key: String, default: Int) =
        sharedPref.getInt(key, default)

    override fun putString(key: String, value: String) {
        sharedPref.edit().putString(key, value).apply()
    }

    override fun getString(key: String, default: String?) =
        sharedPref.getString(key, default)

    override fun putBoolean(key: String, value: Boolean) {
        sharedPref.edit().putBoolean(key, value).apply()
    }

    override fun getBoolean(key: String, default: Boolean) =
        sharedPref.getBoolean(key, default)

    override fun remove(key: String) {
        sharedPref.edit().remove(key).apply()
    }
}
