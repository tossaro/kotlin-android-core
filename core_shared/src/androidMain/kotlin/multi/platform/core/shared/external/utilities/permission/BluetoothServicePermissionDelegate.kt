package multi.platform.core.shared.external.utilities.permission

import android.bluetooth.BluetoothAdapter
import android.provider.Settings
import multi.platform.core.shared.Context
import multi.platform.core.shared.external.enums.PermissionEnum
import multi.platform.core.shared.external.enums.PermissionStateEnum
import multi.platform.core.shared.external.extensions.openPage

internal class BluetoothServicePermissionDelegate(
    private val context: Context,
    private val bluetoothAdapter: BluetoothAdapter?,
) : PermissionDelegate {
    override fun getPermissionState(): PermissionStateEnum {
        return if (bluetoothAdapter?.isEnabled == true) {
            PermissionStateEnum.GRANTED
        } else PermissionStateEnum.DENIED
    }

    override suspend fun providePermission() {
        openSettingPage()
    }

    override fun openSettingPage() {
        context.openPage(
            action = Settings.ACTION_BLUETOOTH_SETTINGS,
            onError = { throw CannotOpenSettingsException(PermissionEnum.BLUETOOTH_SERVICE_ON.name) },
        )
    }
}
