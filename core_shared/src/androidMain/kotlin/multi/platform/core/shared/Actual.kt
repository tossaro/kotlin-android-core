@file:Suppress("UNUSED", "DEPRECATION")

package multi.platform.core.shared

import android.bluetooth.BluetoothManager
import android.content.ContextWrapper
import android.location.LocationManager
import com.google.android.exoplayer2.ExoPlayer
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.okhttp.OkHttp
import multi.platform.core.shared.external.enums.PermissionEnum
import multi.platform.core.shared.external.utilities.LocaleUtil
import multi.platform.core.shared.external.utilities.permission.BluetoothPermissionDelegate
import multi.platform.core.shared.external.utilities.permission.BluetoothServicePermissionDelegate
import multi.platform.core.shared.external.utilities.permission.LocationBackgroundPermissionDelegate
import multi.platform.core.shared.external.utilities.permission.LocationForegroundPermissionDelegate
import multi.platform.core.shared.external.utilities.permission.LocationServicePermissionDelegate
import multi.platform.core.shared.external.utilities.permission.PermissionDelegate
import org.koin.core.qualifier.named
import org.koin.dsl.module
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

actual typealias Bundle = android.os.Bundle
actual typealias Context = ContextWrapper
actual typealias SSLPeerUnverifiedException = javax.net.ssl.SSLPeerUnverifiedException

class AndroidPlatform : Platform {
    var players: MutableList<Pair<String, ExoPlayer>> = mutableListOf()
    override val name: String = "Android ${android.os.Build.VERSION.SDK_INT}"
    override val apiEngine: HttpClientEngine = OkHttp.create()
    override fun player(context: Context?, key: String): ExoPlayer {
        var player = players.find { it.first == key }
        if (player == null) {
            player = Pair(key, ExoPlayer.Builder(context as ContextWrapper).build())
            players.add(player)
        }
        return player.second
    }
}

actual fun getPlatform(): Platform = AndroidPlatform()
actual fun getLanguage(context: Context?): String {
    return context?.let {
        LocaleUtil.retrieveAppLanguage(it)
    } ?: run { "ID" }
}

actual typealias Parcelize = kotlinx.android.parcel.Parcelize
actual interface Parcelable : android.os.Parcelable

@Suppress("SimpleDateFormat")
actual fun formatDate(dateString: String, fromFormat: String, toFormat: String): String {
    val date =
        if (dateString.isNotEmpty()) SimpleDateFormat(fromFormat).parse(dateString) else Date()
    val dateFormatter = SimpleDateFormat(toFormat, Locale.getDefault())
    return dateFormatter.format(date ?: Date())
}

actual fun platformModule() = module {
    single { OkHttp.create() }
    single<PermissionDelegate>(named(PermissionEnum.BLUETOOTH_SERVICE_ON.name)) {
        BluetoothServicePermissionDelegate(get(), get())
    }
    single<PermissionDelegate>(named(PermissionEnum.BLUETOOTH.name)) {
        BluetoothPermissionDelegate(get(), inject())
    }
    single {
        get<Context>().getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
    }
    single {
        get<BluetoothManager>().adapter
    }
    single {
        get<Context>().getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }
    single<PermissionDelegate>(named(PermissionEnum.LOCATION_SERVICE_ON.name)) {
        LocationServicePermissionDelegate(get(), get())
    }
    single<PermissionDelegate>(named(PermissionEnum.LOCATION_FOREGROUND.name)) {
        LocationForegroundPermissionDelegate(get(), inject())
    }
    single<PermissionDelegate>(named(PermissionEnum.LOCATION_BACKGROUND.name)) {
        LocationBackgroundPermissionDelegate(get(), inject(), get(named(PermissionEnum.LOCATION_FOREGROUND.name)))
    }
}

actual class DecimalFormat {
    actual fun format(double: Double, maximumFractionDigits: Int): String {
        val df = java.text.DecimalFormat()
        df.isGroupingUsed = false
        df.maximumFractionDigits = maximumFractionDigits
        df.isDecimalSeparatorAlwaysShown = false
        return df.format(double)
    }
}
