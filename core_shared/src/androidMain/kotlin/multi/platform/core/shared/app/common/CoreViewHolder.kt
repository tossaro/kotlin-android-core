@file:Suppress("kotlin:S1186")

package multi.platform.core.shared.app.common

import android.view.MotionEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.internal.ViewUtils
import multi.platform.core.shared.R
import multi.platform.core.shared.external.enums.StateEnum

abstract class CoreViewHolder<D>(
    val root: View,
    val radius: Int = 0,
    private val elevation: Int = 0,
    private val onSelected: ((View, D) -> Unit)? = null,
    private val onTouch: ((View, MotionEvent) -> Boolean)? = null,
) : RecyclerView.ViewHolder(root) {
    open val shine: View? = null
    open val cardView: CardView? = null
    open fun onSuccess(item: D) {}
    open fun onError() {}
    open fun onEmpty() {}
    open fun onLoading() {}

    @Suppress("RestrictedApi")
    open fun bind(item: D, state: StateEnum? = null) {
        cardView?.let {
            it.radius = ViewUtils.dpToPx(root.context, radius)
            it.elevation = ViewUtils.dpToPx(root.context, elevation)
            if (elevation > 0) it.useCompatPadding = true
        }
        root.apply {
            setOnTouchListener { v, event ->
                if (event.action == MotionEvent.ACTION_UP) v.performClick()
                onTouch?.invoke(v, event) ?: v.onTouchEvent(event)
            }
            setOnClickListener { onSelected?.invoke(this, item) }
        }

        state?.let { s ->
            when (s) {
                StateEnum.LOADING -> {
                    val anim = AnimationUtils.loadAnimation(root.context, R.anim.left_right)
                    shine?.let {
                        it.isVisible = true
                        it.startAnimation(anim)
                    }
                    anim.setAnimationListener(object : Animation.AnimationListener {
                        override fun onAnimationStart(p0: Animation?) {}
                        override fun onAnimationEnd(p0: Animation?) {
                            shine?.startAnimation(anim)
                        }

                        override fun onAnimationRepeat(p0: Animation?) {}
                    })
                    onLoading()
                }
                StateEnum.SUCCESS -> {
                    shine?.isVisible = false
                    onSuccess(item)
                }
                StateEnum.ERROR -> {
                    shine?.isVisible = false
                    onError()
                }
                StateEnum.EMPTY -> {
                    shine?.isVisible = false
                    onEmpty()
                }
            }
        }
    }
}
