@file:Suppress("UNUSED")

package multi.platform.core.shared.external.extensions

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import multi.platform.core.shared.external.enums.PermissionEnum
import multi.platform.core.shared.external.enums.PermissionStateEnum
import multi.platform.core.shared.external.utilities.permission.CannotOpenSettingsException

internal fun Context.openPage(
    action: String,
    newData: Uri? = null,
    onError: (Exception) -> Unit,
) {
    try {
        val intent = Intent(action).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            newData?.let { data = it }
        }
        startActivity(intent)
    } catch (e: Exception) {
        onError(e)
    }
}

internal fun Context.openAppSettingsPage(permission: PermissionEnum) {
    openPage(
        action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
        newData = Uri.parse("package:$packageName"),
        onError = { throw CannotOpenSettingsException(permission.name) },
    )
}

internal fun Context.checkPermissions(
    activity: Lazy<Activity>,
    permissions: List<String>,
): PermissionStateEnum {
    permissions.ifEmpty { return PermissionStateEnum.GRANTED } // no permissions needed
    val status: List<Int> = permissions.map {
        this.checkSelfPermission(it)
    }
    val isAllGranted: Boolean = status.all { it == PackageManager.PERMISSION_GRANTED }
    if (isAllGranted) return PermissionStateEnum.GRANTED

    val isAllRequestRationale: Boolean = try {
        permissions.all {
            !activity.value.shouldShowRequestPermissionRationale(it)
        }
    } catch (t: Throwable) {
        t.printStackTrace()
        true
    }
    return if (isAllRequestRationale) {
        PermissionStateEnum.NOT_DETERMINED
    } else PermissionStateEnum.DENIED
}

fun Context.findActivity(): Activity {
    var context = this
    while (context is ContextWrapper) {
        if (context is Activity) return context
        context = context.baseContext
    }
    throw IllegalStateException("Context of an Activity not found")
}
