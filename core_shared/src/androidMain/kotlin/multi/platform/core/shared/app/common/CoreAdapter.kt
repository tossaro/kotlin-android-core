@file:Suppress("UNUSED")

package multi.platform.core.shared.app.common

import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.internal.ViewUtils
import multi.platform.core.shared.external.enums.StateEnum

abstract class CoreAdapter<D>(
    var widthRatio: Float = 1.0f,
    var height: Int = 0,
    var radius: Int = 0,
    var elevation: Int = 0,
) : RecyclerView.Adapter<CoreViewHolder<D>>() {

    abstract val empty: D
    var items = mutableListOf<D>()
    var itemsCache = mutableListOf<D>()
    var isLastPage = false
    var state = StateEnum.LOADING
    var fetchLimit = 0
    var fetchData: (() -> Unit)? = null
    var onSelected: ((View, D) -> Unit)? = null
    var onClickMore: ((D) -> Unit)? = null
    var onTouch: ((View, MotionEvent) -> Boolean)? = null

    inner class Listener(
        private val swipeRefreshLayout: SwipeRefreshLayout? = null,
        private val fetchArg: (() -> Unit)? = null,
    ) : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            val topRowVerticalPosition =
                if (recyclerView.childCount != 0) recyclerView.getChildAt(0).top else 0
            swipeRefreshLayout?.isEnabled = topRowVerticalPosition >= 0

            if (!recyclerView.canScrollVertically(1) && !isLastPage) {
                fetchArg?.invoke() ?: run { fetchData?.invoke() }
            }
        }
    }

    override fun getItemCount() = items.size

    @Suppress("RestrictedApi")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoreViewHolder<D> {
        val holder = setupHolder(parent, viewType)
        if (widthRatio != 1.0f) {
            holder.root.layoutParams.width =
                (parent.measuredWidth * widthRatio).toInt()
        }
        if (height < 0) {
            holder.root.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        } else if (height > 0) {
            holder.root.layoutParams.height =
                ViewUtils.dpToPx(holder.root.context, height).toInt()
        }
        return holder
    }

    override fun onBindViewHolder(holder: CoreViewHolder<D>, position: Int) {
        holder.bind(items[position], state)
    }

    abstract fun setupHolder(parent: ViewGroup, viewType: Int): CoreViewHolder<D>

    fun clear() {
        val size = items.size
        items.clear()
        itemsCache.clear()
        state = StateEnum.LOADING
        notifyItemRangeRemoved(0, size)
    }

    fun showSkeletons(size: Int = fetchLimit) {
        clear()
        val skeletons = mutableListOf<D>()
        for (i in 0 until size) skeletons.add(empty)
        items = skeletons
        notifyItemRangeInserted(0, skeletons.size - 1)
    }

    fun addItem(process: () -> Unit) {
        if (itemsCache.isEmpty() && items.isNotEmpty()) clear()
        val sizeBefore = items.size
        process.invoke()
        val sizeAfter = items.size
        if ((sizeAfter - sizeBefore) < fetchLimit) isLastPage = true
        when (sizeAfter) {
            0 -> {
                state = StateEnum.EMPTY
                items = mutableListOf(empty)
                notifyItemInserted(0)
            }
            1 -> {
                state = StateEnum.SUCCESS
                notifyItemInserted(0)
            }
            else -> {
                state = StateEnum.SUCCESS
                notifyItemRangeInserted(sizeBefore, sizeAfter - 1)
            }
        }
    }

    fun showError() {
        clear()
        state = StateEnum.ERROR
        items = mutableListOf(empty)
        notifyItemInserted(0)
    }
}
