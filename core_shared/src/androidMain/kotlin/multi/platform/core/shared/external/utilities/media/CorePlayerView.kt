@file:Suppress("UNUSED", "DEPRECATION")

package multi.platform.core.shared.external.utilities.media

import android.content.Context
import android.graphics.SurfaceTexture
import android.util.AttributeSet
import android.view.Surface
import android.view.SurfaceView
import android.view.TextureView
import com.google.android.exoplayer2.ui.PlayerView

class CorePlayerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : PlayerView(context, attrs, defStyleAttr) {

    companion object {
        const val SURFACE_TYPE_NONE = 0
        const val SURFACE_TYPE_SURFACE_VIEW = 1
        const val SURFACE_TYPE_TEXTURE_VIEW = 2
    }

    private var customSurfaceType = SURFACE_TYPE_SURFACE_VIEW // Default to SurfaceView

    fun setSurfaceType(surfaceType: Int) {
        customSurfaceType = surfaceType
        initializeSurface()
    }

    private fun initializeSurface() {
        removeAllViews()
        val newSurfaceView = when (customSurfaceType) {
            SURFACE_TYPE_TEXTURE_VIEW -> {
                val textureView = TextureView(context).apply {
                    layoutParams = LayoutParams(
                        LayoutParams.MATCH_PARENT,
                        LayoutParams.MATCH_PARENT,
                    )
                }
                textureView.surfaceTextureListener = object : TextureView.SurfaceTextureListener {
                    override fun onSurfaceTextureAvailable(
                        surfaceTexture: SurfaceTexture,
                        width: Int,
                        height: Int,
                    ) {
                        val surface = Surface(surfaceTexture)
                        player?.setVideoSurface(surface)
                    }

                    override fun onSurfaceTextureSizeChanged(
                        surfaceTexture: SurfaceTexture,
                        width: Int,
                        height: Int,
                    ) {
                        // no need implementation
                    }

                    override fun onSurfaceTextureDestroyed(surfaceTexture: SurfaceTexture): Boolean {
                        player?.setVideoSurface(null)
                        return true
                    }

                    override fun onSurfaceTextureUpdated(surfaceTexture: SurfaceTexture) {
                        // no need implementation
                    }
                }
                textureView
            }
            SURFACE_TYPE_SURFACE_VIEW -> SurfaceView(context).apply {
                layoutParams = LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT,
                )
            }
            else -> null
        }

        newSurfaceView?.let {
            addView(it, 0)
            player?.setVideoSurfaceView(it as? SurfaceView)
            player?.setVideoTextureView(it as? TextureView)
        }
    }
}
