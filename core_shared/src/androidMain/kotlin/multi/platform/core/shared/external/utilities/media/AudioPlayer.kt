@file:Suppress("UNUSED", "DEPRECATION")

package multi.platform.core.shared.external.utilities.media

import android.os.Handler
import android.os.Looper
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import multi.platform.core.shared.app.CoreApplication

actual class AudioPlayer actual constructor(private val playerState: PlayerState) : Runnable {

    private val handler = Handler(Looper.getMainLooper())
    private val mediaPlayer = ExoPlayer.Builder(CoreApplication.appContext).build()
    private var mediaItems = mutableListOf<MediaItem>()
    private var currentItemIndex = -1

    private val listener = object : Player.Listener {
        override fun onPlaybackStateChanged(playbackState: Int) {
            when (playbackState) {
                Player.STATE_IDLE -> {
                    // no need implementation
                }

                Player.STATE_BUFFERING -> {
                    playerState.isBuffering = true
                }

                Player.STATE_ENDED -> {
                    if (playerState.isPlaying) {
                        next()
                    }
                }

                Player.STATE_READY -> {
                    playerState.isBuffering = false
                    playerState.duration = mediaPlayer.duration / 1000
                }
            }
        }

        override fun onIsPlayingChanged(isPlaying: Boolean) {
            playerState.isPlaying = isPlaying
            if (isPlaying) {
                scheduleUpdate()
            } else {
                stopUpdate()
            }
        }
    }

    private fun stopUpdate() {
        handler.removeCallbacks(this)
    }

    private fun scheduleUpdate() {
        stopUpdate()
        handler.postDelayed(this, 100)
    }

    actual fun play() {
        if (currentItemIndex == -1) {
            play(0)
        } else {
            mediaPlayer.play()
        }
    }

    actual fun pause() {
        mediaPlayer.pause()
    }

    actual fun addSongsUrls(songsUrl: List<String>) {
        mediaItems += songsUrl.map { MediaItem.fromUri(it) }
        mediaPlayer.removeListener(listener)
        mediaPlayer.addListener(listener)
        mediaPlayer.pauseAtEndOfMediaItems = true
        mediaPlayer.prepare()
    }

    actual fun replaceSongsUrls(songsUrl: List<String>) {
        mediaItems = songsUrl.map { MediaItem.fromUri(it) }.toMutableList()
        mediaPlayer.removeListener(listener)
        mediaPlayer.addListener(listener)
        mediaPlayer.pauseAtEndOfMediaItems = true
        mediaPlayer.prepare()
    }

    actual fun next() {
        playerState.canNext = (currentItemIndex + 1) < mediaItems.size
        if (playerState.canNext) {
            currentItemIndex += 1
            playWithIndex(currentItemIndex)
        }
    }

    actual fun prev() {
        when {
            playerState.currentTime > 3 -> {
                seekTo(0.0)
            }

            else -> {
                playerState.canPrev = (currentItemIndex - 1) >= 0
                if (playerState.canPrev) {
                    currentItemIndex -= 1
                    playWithIndex(currentItemIndex)
                }
            }
        }
    }

    actual fun play(songIndex: Int) {
        if (songIndex < mediaItems.size) {
            currentItemIndex = songIndex
            playWithIndex(currentItemIndex)
        }
    }

    actual fun seekTo(time: Double) {
        val seekTime = time * 1000
        mediaPlayer.seekTo(seekTime.toLong())
    }

    private fun playWithIndex(index: Int) {
        playerState.currentItemIndex = index
        val playItem = mediaItems[index]
        mediaPlayer.setMediaItem(playItem)
        mediaPlayer.play()
    }

    override fun run() {
        playerState.currentTime = mediaPlayer.currentPosition / 1000
        handler.postDelayed(this, 1000)
    }

    actual fun cleanUp() {
        mediaPlayer.release()
        mediaPlayer.removeListener(listener)
    }
}
