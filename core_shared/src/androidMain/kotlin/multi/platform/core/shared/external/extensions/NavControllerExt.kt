@file:Suppress("UNUSED")

package multi.platform.core.shared.external.extensions

import androidx.navigation.NavController

/**
 * Extension for check route id in back stack
 */
fun NavController.isInBackStack(destinationId: Int) =
    try {
        getBackStackEntry(destinationId)
        true
    } catch (e: Exception) {
        false
    }
