@file:Suppress("UNUSED", "DEPRECATION")

package multi.platform.core.shared.external.utilities.media

import android.content.Context
import android.net.Uri
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.google.ads.interactivemedia.v3.api.AdEvent
import com.google.ads.interactivemedia.v3.api.ImaSdkFactory
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.ads.AdsLoader
import com.google.android.exoplayer2.source.ads.AdsMediaSource
import com.google.android.exoplayer2.ui.AdViewProvider
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import multi.platform.core.shared.getPlatform
import multi.platform.core.shared.Context as CoreContext

@Composable
actual fun AdsPlayer(modifier: Modifier, key: String, videoPlayerConfig: VideoPlayerConfig) {
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    var adsLoader: AdsLoader? = null
    var videoListener: Player.Listener? = null
    val exoPlayer = remember {
        (getPlatform().player(context as CoreContext, key) as? ExoPlayer)?.apply {
            videoPlayerConfig.volume?.let { volume = it }
            repeatMode = videoPlayerConfig.repeatMode
        }
    }

    DisposableEffect(lifecycleOwner, exoPlayer) {
        val observer = LifecycleEventObserver { _, event ->
            when (event) {
                Lifecycle.Event.ON_RESUME -> exoPlayer?.playWhenReady = videoPlayerConfig.isAutoPlay
                Lifecycle.Event.ON_PAUSE -> exoPlayer?.playWhenReady = false
                Lifecycle.Event.ON_DESTROY -> exoPlayer?.release()
                else -> {
                    // no need implementation
                }
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
            videoListener?.let { exoPlayer?.removeListener(it) }
            exoPlayer?.release()
            exoPlayer?.stop()
            adsLoader?.release()
        }
    }
    AndroidView(
        modifier = modifier,
        factory = { ctx ->
            val playerView = PlayerView(ctx).apply {
                useController = videoPlayerConfig.useController
                resizeMode = videoPlayerConfig.resizeMode
                setShowBuffering(videoPlayerConfig.showBufferMode)
                player = exoPlayer
            }
            videoListener = object : Player.Listener {
                override fun onPlaybackStateChanged(playbackState: Int) {
                    videoPlayerConfig.onPlaybackStateChanged?.invoke(playbackState)
                    if (playbackState == Player.STATE_IDLE) {
                        adsLoader?.release()
                        adsLoader = setupAdsLoader(ctx, videoPlayerConfig, exoPlayer, playerView)
                    }
                }
            }
            adsLoader = setupAdsLoader(ctx, videoPlayerConfig, exoPlayer, playerView)
            playerView
        },
        update = {
            exoPlayer?.playWhenReady = videoPlayerConfig.isAutoPlay
            if (exoPlayer?.playWhenReady == true) {
                videoListener?.let { exoPlayer.addListener(it) }
                println("exoPlayer.listener.add")
            } else {
                videoListener?.let { exoPlayer?.removeListener(it) }
                println("exoPlayer.listener.remove")
            }
            videoPlayerConfig.volume?.let { exoPlayer?.volume = it }
        },
    )
}

private fun setupAdsLoader(
    context: Context,
    videoPlayerConfig: VideoPlayerConfig,
    exoPlayer: ExoPlayer?,
    playerView: AdViewProvider,
): AdsLoader {
    val adsListener = AdEvent.AdEventListener { adEvent ->
        videoPlayerConfig.adEventListener?.invoke(adEvent)
    }
    val adsLoader = ImaAdsLoader.Builder(context)
        .setImaSdkSettings(ImaSdkFactory.getInstance().createImaSdkSettings())
        .setAdEventListener(adsListener)
        .build()
    adsLoader.setPlayer(exoPlayer)
    val defaultDataSourceFactory = DefaultDataSourceFactory(context, "AdsPlayer")
    val adsMediaSource = AdsMediaSource(
        ProgressiveMediaSource.Factory(defaultDataSourceFactory).createMediaSource(
            MediaItem.fromUri(videoPlayerConfig.urls[0]),
        ),
        DataSpec(Uri.parse(videoPlayerConfig.urls[0])),
        "",
        DefaultMediaSourceFactory(defaultDataSourceFactory),
        adsLoader,
        playerView,
    )
    exoPlayer?.prepare(adsMediaSource)
    return adsLoader
}
