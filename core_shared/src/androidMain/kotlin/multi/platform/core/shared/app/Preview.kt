package multi.platform.core.shared.app

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import multi.platform.core.shared.app.common.compossables.CoreLoadingView
import multi.platform.core.shared.app.common.compossables.CoreTextField

@Preview(showBackground = true)
@Composable
fun CoreLoadingViewLinearPreview() {
    CoreLoadingView(isLoading = true)
}

@Preview(showBackground = true)
@Composable
fun CoreLoadingViewCurcularPreview() {
    CoreLoadingView(isLoading = true, isLinear = false)
}

@Preview(showBackground = true)
@Composable
fun CoreTextFieldNormalPreview() {
    CoreTextField(title = "Title", value = "Value", onValueChange = {}, label = {})
}

@Preview(showBackground = true)
@Composable
fun CoreTextFieldErrorPreview() {
    CoreTextField(title = "Title", value = "Value", onValueChange = {}, label = {}, validation = { "Error" })
}
