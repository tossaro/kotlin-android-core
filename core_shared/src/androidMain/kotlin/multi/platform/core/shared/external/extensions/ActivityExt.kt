package multi.platform.core.shared.external.extensions

import android.app.Activity
import androidx.core.app.ActivityCompat

internal fun Activity.providePermissions(
    permissions: List<String>,
    onError: (Throwable) -> Unit,
) {
    try {
        ActivityCompat.requestPermissions(
            this,
            permissions.toTypedArray(),
            100,
        )
    } catch (t: Throwable) {
        onError(t)
    }
}
