// package multi.platform.core.shared.app.errorconnectiondialog
//
// import android.os.Bundle
// import android.view.LayoutInflater
// import android.view.View
// import android.view.ViewGroup
// import androidx.core.os.bundleOf
// import androidx.databinding.DataBindingUtil
// import androidx.fragment.app.setFragmentResult
// import multi.platform.core.shared.R
// import multi.platform.core.shared.app.common.CoreDialogFragment
// import multi.platform.core.shared.databinding.ErrorConnectionDialogFragmentBinding
// import multi.platform.core.shared.external.constants.CommonKey
//
// class ErrorConnectionDialogFragment : CoreDialogFragment() {
//    private lateinit var binding: ErrorConnectionDialogFragmentBinding
//    override fun isCancelable() = true
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?,
//    ): View {
//        binding = DataBindingUtil.inflate(inflater, R.layout.error_connection_dialog_fragment, container, false)
//        return binding.root
//    }
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        binding.mbErrorConnectionRetry.setOnClickListener {
//            setFragmentResult(
//                CommonKey.RETRY_KEY,
//                bundleOf(CommonKey.RETRY_KEY to arguments?.getString("key", null)),
//            )
//            dismiss()
//        }
//    }
// }
