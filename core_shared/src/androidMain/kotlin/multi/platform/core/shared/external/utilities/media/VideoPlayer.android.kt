@file:Suppress("UNUSED", "DEPRECATION")

package multi.platform.core.shared.external.utilities.media

import android.graphics.Matrix
import android.graphics.SurfaceTexture
import android.view.Surface
import android.view.TextureView
import android.view.ViewGroup
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView

@Composable
actual fun VideoPlayer(modifier: Modifier, videoPlayerConfig: VideoPlayerConfig) {
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    val exoPlayer = remember {
        ExoPlayer.Builder(context).build().apply {
            videoPlayerConfig.volume?.let { volume = it }
            setMediaItems(videoPlayerConfig.urls.map { MediaItem.fromUri(it) })
            seekToDefaultPosition(videoPlayerConfig.position)
            repeatMode = videoPlayerConfig.repeatMode
            prepare()
        }
    }
    DisposableEffect(lifecycleOwner, exoPlayer) {
        val observer = LifecycleEventObserver { _, event ->
            when (event) {
                Lifecycle.Event.ON_RESUME -> exoPlayer.playWhenReady = true
                Lifecycle.Event.ON_PAUSE -> exoPlayer.playWhenReady = false
                Lifecycle.Event.ON_DESTROY -> exoPlayer.release()
                else -> {
                    // no need implementation
                }
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
            exoPlayer.release()
        }
    }
    AndroidView(
        modifier = modifier,
        factory = { ctx ->
            PlayerView(ctx).apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                )
                useController = videoPlayerConfig.useController
                resizeMode = videoPlayerConfig.resizeMode
                setShowBuffering(videoPlayerConfig.showBufferMode)
                player = exoPlayer
                setCustomTextureView(videoPlayerConfig.resizeMode)
            }
        },
        update = {
            exoPlayer.playWhenReady = true
        },
    )
}

private fun PlayerView.setCustomTextureView(resizeMode: Int) {
    val textureView = TextureView(context).apply {
        layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT,
        )
    }
    val aspectFrameLayout = AspectRatioFrameLayout(context).apply {
        layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT,
        )
        setResizeMode(resizeMode)
    }
    removeView(videoSurfaceView)
    aspectFrameLayout.addView(textureView)
    addView(aspectFrameLayout)

    textureView.surfaceTextureListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureAvailable(
            surfaceTexture: SurfaceTexture,
            width: Int,
            height: Int,
        ) {
            val surface = Surface(surfaceTexture)
            player?.setVideoSurface(surface)

            if (resizeMode == AspectRatioFrameLayout.RESIZE_MODE_ZOOM) {
                val videoAspectRatio = if (player?.videoSize?.height == 0) {
                    1f
                } else player?.videoSize?.width?.toFloat()!! / player?.videoSize?.height!!
                val viewAspectRatio = width.toFloat() / height
                val scaleX: Float
                val scaleY: Float

                if (videoAspectRatio > viewAspectRatio) {
                    scaleX = videoAspectRatio / viewAspectRatio
                    scaleY = 1f
                } else {
                    scaleX = 1f
                    scaleY = viewAspectRatio / videoAspectRatio
                }

                textureView.setTransform(
                    Matrix().apply {
                        setScale(scaleX, scaleY, width / 2f, height / 2f)
                    },
                )
            }
        }

        override fun onSurfaceTextureSizeChanged(
            surfaceTexture: SurfaceTexture,
            width: Int,
            height: Int,
        ) {
            // no need implementation
        }

        override fun onSurfaceTextureDestroyed(surfaceTexture: SurfaceTexture): Boolean {
            player?.setVideoSurface(null)
            return true
        }

        override fun onSurfaceTextureUpdated(surfaceTexture: SurfaceTexture) {
            // no need implementation
        }
    }
}
