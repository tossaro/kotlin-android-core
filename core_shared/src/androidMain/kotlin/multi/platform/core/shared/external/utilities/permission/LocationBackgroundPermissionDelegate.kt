package multi.platform.core.shared.external.utilities.permission

import android.Manifest
import android.app.Activity
import android.content.Context
import android.os.Build
import multi.platform.core.shared.external.enums.PermissionEnum
import multi.platform.core.shared.external.enums.PermissionStateEnum
import multi.platform.core.shared.external.extensions.checkPermissions
import multi.platform.core.shared.external.extensions.openAppSettingsPage
import multi.platform.core.shared.external.extensions.providePermissions

internal class LocationBackgroundPermissionDelegate(
    private val context: Context,
    private val activity: Lazy<Activity>,
    private val locationForegroundPermissionDelegate: PermissionDelegate,
) : PermissionDelegate {
    override fun getPermissionState(): PermissionStateEnum {
        return when (locationForegroundPermissionDelegate.getPermissionState()) {
            PermissionStateEnum.GRANTED ->
                context.checkPermissions(activity, backgroundLocationPermissions)

            PermissionStateEnum.DENIED,
            PermissionStateEnum.NOT_DETERMINED,
            -> PermissionStateEnum.NOT_DETERMINED
        }
    }

    override suspend fun providePermission() {
        activity.value.providePermissions(backgroundLocationPermissions) {
            throw Exception(
                it.localizedMessage ?: "Failed to request background location permission",
            )
        }
        getPermissionState()
    }

    override fun openSettingPage() {
        context.openAppSettingsPage(PermissionEnum.LOCATION_BACKGROUND)
    }
}

private val backgroundLocationPermissions: List<String> =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        listOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
    } else {
        emptyList()
    }
