@file:Suppress("UNUSED")

package multi.platform.core.shared.external.utilities.media

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import platform.AVFoundation.AVPlayer
import platform.AVFoundation.AVPlayerLayer
import platform.Foundation.NSURL

@Composable
actual fun VideoPlayer(modifier: Modifier, videoPlayerConfig: VideoPlayerConfig) {
    val player = remember { AVPlayer(uRL = NSURL.URLWithString(videoPlayerConfig.urls[videoPlayerConfig.position])!!) }
    val playerLayer = remember { AVPlayerLayer() }
    playerLayer.player = player
    /**
     val avPlayerViewController = remember { AVPlayerViewController() }
     avPlayerViewController.player = player
     avPlayerViewController.showsPlaybackControls = true

     // Use a UIKitView to integrate with your existing UIKit views
     UIKitView(
     factory = {
     // Create a UIView to hold the AVPlayerLayer
     val playerContainer = UIView()
     playerContainer.addSubview(avPlayerViewController.view)
     // Return the playerContainer as the root UIView
     playerContainer
     },
     onResize = { view: UIView, rect: CValue<CGRect> ->
     CATransaction.begin()
     CATransaction.setValue(true, kCATransactionDisableActions)
     view.layer.setFrame(rect)
     playerLayer.setFrame(rect)
     avPlayerViewController.view.layer.frame = rect
     CATransaction.commit()
     },
     update = { view ->
     player.play()
     avPlayerViewController.player!!.play()
     },
     modifier = modifier
     )
     **/
}
