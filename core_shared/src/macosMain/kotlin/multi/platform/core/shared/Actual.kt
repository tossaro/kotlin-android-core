@file:Suppress("UNUSED", "kotlin:S1172")

package multi.platform.core.shared

import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.darwin.Darwin
import org.koin.dsl.module
import platform.Foundation.NSDate
import platform.Foundation.NSDateFormatter
import platform.Foundation.NSLocale
import platform.Foundation.NSNumber
import platform.Foundation.NSNumberFormatter
import platform.Foundation.NSProcessInfo
import platform.Foundation.countryCode
import platform.Foundation.currentLocale

actual typealias Bundle = platform.darwin.NSObject
actual typealias Context = platform.darwin.NSObject

class MacOSPlatform : Platform {
    override val name: String =
        NSProcessInfo.processInfo().operatingSystemName() + " " + NSProcessInfo.processInfo().operatingSystemVersionString()
    override val apiEngine: HttpClientEngine = Darwin.create()
}

actual fun getPlatform(): Platform = MacOSPlatform()

actual fun getLanguage(context: Context?) = NSLocale.currentLocale().countryCode?.uppercase() ?: "ID"

actual interface Parcelable

actual fun formatDate(dateString: String, fromFormat: String, toFormat: String): String {
    val dateFormatter = NSDateFormatter().apply {
        dateFormat = fromFormat
    }
    val formatter = NSDateFormatter().apply {
        dateFormat = toFormat
        locale = NSLocale(localeIdentifier = "id_ID")
    }
    return formatter.stringFromDate(dateFormatter.dateFromString(dateString) ?: NSDate())
}

actual fun platformModule() = module {
    single { Darwin.create() }
}

actual class DecimalFormat {
    actual fun format(double: Double, maximumFractionDigits: Int): String {
        val formatter = NSNumberFormatter()
        formatter.minimumFractionDigits = 0u
        formatter.maximumFractionDigits = maximumFractionDigits.toULong()
        formatter.numberStyle = 1u // Decimal
        return formatter.stringFromNumber(NSNumber(double))!!
    }
}
