@file:Suppress("UNUSED")

package multi.platform.core.shared.external.utilities

import platform.Foundation.NSUserDefaults

class DefaultPersistentImpl : Persistent {
    override fun putInt(key: String, value: Int) {
        NSUserDefaults.standardUserDefaults.setInteger(value.toLong(), key)
    }

    override fun getInt(key: String, default: Int) =
        NSUserDefaults.standardUserDefaults.integerForKey(key).toInt()

    override fun putString(key: String, value: String) {
        NSUserDefaults.standardUserDefaults.setObject(value, key)
    }

    override fun getString(key: String, default: String?) =
        NSUserDefaults.standardUserDefaults.stringForKey(key)

    override fun putBoolean(key: String, value: Boolean) {
        NSUserDefaults.standardUserDefaults.setBool(value, key)
    }

    override fun getBoolean(key: String, default: Boolean) =
        NSUserDefaults.standardUserDefaults.boolForKey(key)

    override fun remove(key: String) {
        NSUserDefaults.standardUserDefaults.removeObjectForKey(key)
    }
}
