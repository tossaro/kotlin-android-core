package multi.platform.core.shared.external.utilities.permission

import multi.platform.core.shared.external.enums.PermissionStateEnum
import multi.platform.core.shared.external.utilities.openAppSettingsPage
import platform.CoreLocation.CLLocationManager
import platform.CoreLocation.kCLAuthorizationStatusAuthorizedAlways
import platform.CoreLocation.kCLAuthorizationStatusAuthorizedWhenInUse
import platform.CoreLocation.kCLAuthorizationStatusDenied
import platform.CoreLocation.kCLAuthorizationStatusNotDetermined
import platform.CoreLocation.kCLAuthorizationStatusRestricted

internal class LocationForegroundPermissionDelegate : PermissionDelegate {
    private var locationManager = CLLocationManager()

    override fun getPermissionState(): PermissionStateEnum {
        return when (locationManager.authorizationStatus()) {
            kCLAuthorizationStatusAuthorizedAlways,
            kCLAuthorizationStatusAuthorizedWhenInUse,
            kCLAuthorizationStatusRestricted,
            -> PermissionStateEnum.GRANTED

            kCLAuthorizationStatusNotDetermined -> PermissionStateEnum.NOT_DETERMINED
            kCLAuthorizationStatusDenied -> PermissionStateEnum.DENIED
            else -> PermissionStateEnum.NOT_DETERMINED
        }
    }

    override suspend fun providePermission() {
        locationManager.requestWhenInUseAuthorization()
    }

    override fun openSettingPage() {
        openAppSettingsPage()
    }
}
