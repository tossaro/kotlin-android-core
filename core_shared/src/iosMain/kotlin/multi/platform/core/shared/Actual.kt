@file:Suppress("UNUSED", "kotlin:S1172")

package multi.platform.core.shared

import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.darwin.Darwin
import multi.platform.core.shared.external.enums.PermissionEnum
import multi.platform.core.shared.external.utilities.permission.BluetoothPermissionDelegate
import multi.platform.core.shared.external.utilities.permission.BluetoothServicePermissionDelegate
import multi.platform.core.shared.external.utilities.permission.LocationBackgroundPermissionDelegate
import multi.platform.core.shared.external.utilities.permission.LocationForegroundPermissionDelegate
import multi.platform.core.shared.external.utilities.permission.LocationServicePermissionDelegate
import multi.platform.core.shared.external.utilities.permission.PermissionDelegate
import org.koin.core.qualifier.named
import org.koin.dsl.module
import platform.AVFoundation.AVPlayer
import platform.Foundation.NSDate
import platform.Foundation.NSDateFormatter
import platform.Foundation.NSError
import platform.Foundation.NSLocale
import platform.Foundation.NSNumber
import platform.Foundation.NSNumberFormatter
import platform.Foundation.countryCode
import platform.Foundation.currentLocale
import platform.UIKit.UIDevice

actual typealias Bundle = platform.darwin.NSObject
actual typealias Context = platform.darwin.NSObject
actual typealias SSLPeerUnverifiedException = NSError

class IOSPlatform : Platform {
    private var players: MutableList<Pair<String, AVPlayer>> = mutableListOf()
    override val name: String =
        UIDevice.currentDevice.systemName() + " " + UIDevice.currentDevice.systemVersion
    override val apiEngine: HttpClientEngine = Darwin.create()
    override fun player(context: Context?, key: String): AVPlayer? {
        val player = players.find { it.first == key }
        if (player == null) players.add(key to AVPlayer())
        return player?.second
    }
}

actual fun getPlatform(): Platform = IOSPlatform()
actual fun getLanguage(context: Context?) = NSLocale.currentLocale().countryCode?.uppercase() ?: "ID"

actual interface Parcelable

actual fun formatDate(dateString: String, fromFormat: String, toFormat: String): String {
    val dateFormatter = NSDateFormatter().apply {
        dateFormat = fromFormat
    }
    val formatter = NSDateFormatter().apply {
        dateFormat = toFormat
        locale = NSLocale(localeIdentifier = "id_ID")
    }
    return formatter.stringFromDate(dateFormatter.dateFromString(dateString) ?: NSDate())
}

actual fun platformModule() = module {
    single { Darwin.create() }
    single<PermissionDelegate>(named(PermissionEnum.BLUETOOTH_SERVICE_ON.name)) {
        BluetoothServicePermissionDelegate()
    }
    single<PermissionDelegate>(named(PermissionEnum.BLUETOOTH.name)) {
        BluetoothPermissionDelegate()
    }
    single<PermissionDelegate>(named(PermissionEnum.LOCATION_SERVICE_ON.name)) {
        LocationServicePermissionDelegate()
    }
    single<PermissionDelegate>(named(PermissionEnum.LOCATION_FOREGROUND.name)) {
        LocationForegroundPermissionDelegate()
    }
    single<PermissionDelegate>(named(PermissionEnum.LOCATION_BACKGROUND.name)) {
        LocationBackgroundPermissionDelegate(
            locationForegroundPermissionDelegate = get(named(PermissionEnum.LOCATION_FOREGROUND.name)),
        )
    }
}

actual class DecimalFormat {
    actual fun format(double: Double, maximumFractionDigits: Int): String {
        val formatter = NSNumberFormatter()
        formatter.minimumFractionDigits = 0u
        formatter.maximumFractionDigits = maximumFractionDigits.toULong()
        formatter.numberStyle = 1u // Decimal
        return formatter.stringFromNumber(NSNumber(double))!!
    }
}
