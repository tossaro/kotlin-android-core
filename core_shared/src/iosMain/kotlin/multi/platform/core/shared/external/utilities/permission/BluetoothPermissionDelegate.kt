package multi.platform.core.shared.external.utilities.permission

import multi.platform.core.shared.external.enums.PermissionStateEnum
import multi.platform.core.shared.external.utilities.openAppSettingsPage
import platform.CoreBluetooth.CBCentralManager
import platform.CoreBluetooth.CBManagerAuthorizationAllowedAlways
import platform.CoreBluetooth.CBManagerAuthorizationDenied
import platform.CoreBluetooth.CBManagerAuthorizationNotDetermined
import platform.CoreBluetooth.CBManagerAuthorizationRestricted

internal class BluetoothPermissionDelegate : PermissionDelegate {
    override fun getPermissionState(): PermissionStateEnum {
        return when (CBCentralManager.authorization) {
            CBManagerAuthorizationNotDetermined -> PermissionStateEnum.NOT_DETERMINED
            CBManagerAuthorizationAllowedAlways, CBManagerAuthorizationRestricted -> PermissionStateEnum.GRANTED
            CBManagerAuthorizationDenied -> PermissionStateEnum.DENIED
            else -> PermissionStateEnum.NOT_DETERMINED
        }
    }

    override suspend fun providePermission() {
        CBCentralManager().authorization()
    }

    override fun openSettingPage() {
        openAppSettingsPage()
    }
}
