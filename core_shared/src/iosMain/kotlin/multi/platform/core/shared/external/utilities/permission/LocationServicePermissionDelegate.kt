package multi.platform.core.shared.external.utilities.permission

import multi.platform.core.shared.external.enums.PermissionStateEnum
import multi.platform.core.shared.external.utilities.openNSUrl
import platform.CoreLocation.CLLocationManager

internal class LocationServicePermissionDelegate : PermissionDelegate {
    private val locationManager = CLLocationManager()

    override fun getPermissionState(): PermissionStateEnum {
        return if (locationManager.locationServicesEnabled()) {
            PermissionStateEnum.GRANTED
        } else PermissionStateEnum.DENIED
    }

    override suspend fun providePermission() {
        openSettingPage()
    }

    override fun openSettingPage() {
        openNSUrl("App-Prefs:Privacy&path=LOCATION")
    }
}
