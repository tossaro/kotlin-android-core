package multi.platform.core.shared.external.utilities.permission

import multi.platform.core.shared.external.enums.PermissionStateEnum
import multi.platform.core.shared.external.utilities.openAppSettingsPage
import platform.CoreLocation.CLLocationManager
import platform.CoreLocation.kCLAuthorizationStatusAuthorizedAlways
import platform.CoreLocation.kCLAuthorizationStatusDenied

internal class LocationBackgroundPermissionDelegate(
    private val locationForegroundPermissionDelegate: PermissionDelegate,
) : PermissionDelegate {
    override fun getPermissionState(): PermissionStateEnum {
        val foregroundPermissionStatus =
            locationForegroundPermissionDelegate.getPermissionState()
        return when (foregroundPermissionStatus) {
            PermissionStateEnum.GRANTED -> checkBackgroundLocationPermission()
            PermissionStateEnum.DENIED,
            PermissionStateEnum.NOT_DETERMINED,
            -> foregroundPermissionStatus
        }
    }

    override suspend fun providePermission() {
        CLLocationManager().requestAlwaysAuthorization()
    }

    override fun openSettingPage() {
        openAppSettingsPage()
    }

    private fun checkBackgroundLocationPermission(): PermissionStateEnum {
        return when (CLLocationManager.authorizationStatus()) {
            kCLAuthorizationStatusAuthorizedAlways -> PermissionStateEnum.GRANTED
            kCLAuthorizationStatusDenied -> PermissionStateEnum.DENIED
            else -> PermissionStateEnum.NOT_DETERMINED
        }
    }
}
