package multi.platform.core.shared.external.utilities.permission

import multi.platform.core.shared.external.enums.PermissionStateEnum
import multi.platform.core.shared.external.utilities.openNSUrl
import platform.CoreBluetooth.CBCentralManager
import platform.CoreBluetooth.CBCentralManagerDelegateProtocol
import platform.CoreBluetooth.CBManagerAuthorizationAllowedAlways
import platform.CoreBluetooth.CBManagerAuthorizationRestricted
import platform.CoreBluetooth.CBManagerStatePoweredOn
import platform.darwin.NSObject

internal class BluetoothServicePermissionDelegate : PermissionDelegate {
    private val cbCentralManager: CBCentralManager by lazy {
        CBCentralManager(
            object : NSObject(), CBCentralManagerDelegateProtocol {
                override fun centralManagerDidUpdateState(central: CBCentralManager) {
                    // no need implementation
                }
            },
            null,
        )
    }

    override fun getPermissionState(): PermissionStateEnum {
        val hasBluetoothPermissionGranted =
            CBCentralManager.authorization == CBManagerAuthorizationAllowedAlways ||
                CBCentralManager.authorization == CBManagerAuthorizationRestricted
        return if (hasBluetoothPermissionGranted) {
            if (cbCentralManager.state() == CBManagerStatePoweredOn) {
                PermissionStateEnum.GRANTED
            } else PermissionStateEnum.DENIED
        } else PermissionStateEnum.NOT_DETERMINED
    }

    override suspend fun providePermission() {
        openSettingPage()
    }

    override fun openSettingPage() {
        openNSUrl("App-Prefs:Bluetooth")
    }
}
