package multi.platform.core.shared.external.utilities.media

actual class AudioPlayer actual constructor(private val playerState: PlayerState) : Runnable {

    actual fun play() {
        throw NotImplementedError()
    }

    actual fun pause() {
        throw NotImplementedError()
    }

    actual fun addSongsUrls(songsUrl: List<String>) {
        throw NotImplementedError()
    }

    actual fun replaceSongsUrls(songsUrl: List<String>) {
        throw NotImplementedError()
    }

    actual fun next() {
        throw NotImplementedError()
    }

    actual fun prev() {
        throw NotImplementedError()
    }

    actual fun play(songIndex: Int) {
        throw NotImplementedError()
    }

    actual fun seekTo(time: Double) {
        throw NotImplementedError()
    }

    private fun playWithIndex(index: Int) {
        throw NotImplementedError()
    }

    override fun run() {
        throw NotImplementedError()
    }

    actual fun cleanUp() {
        throw NotImplementedError()
    }
}
