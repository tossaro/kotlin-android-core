@file:Suppress("UNUSED", "kotlin:S1172")

package multi.platform.core.shared

import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.okhttp.OkHttp
import org.koin.dsl.module
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

actual typealias Context = NotImplementedError

class DesktopPlatform : Platform {
    override val name: String = System.getProperty("os.name")
    override val apiEngine: HttpClientEngine = OkHttp.create()
}

actual fun getPlatform(): Platform = DesktopPlatform()
actual fun getLanguage(context: Context?) = "ID"

actual interface Parcelable

@Suppress("SimpleDateFormat")
actual fun formatDate(dateString: String, fromFormat: String, toFormat: String): String {
    val date =
        if (dateString.isNotEmpty()) SimpleDateFormat(fromFormat).parse(dateString) else Date()
    val dateFormatter = SimpleDateFormat(toFormat, Locale.getDefault())
    return dateFormatter.format(date ?: Date())
}

actual fun platformModule() = module {
    single { OkHttp.create() }
}

actual class DecimalFormat {
    actual fun format(double: Double, maximumFractionDigits: Int): String {
        val df = java.text.DecimalFormat()
        df.isGroupingUsed = false
        df.maximumFractionDigits = maximumFractionDigits
        df.isDecimalSeparatorAlwaysShown = false
        return df.format(double)
    }
}
