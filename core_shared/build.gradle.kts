@file:Suppress("UnstableApiUsage")
import org.jetbrains.kotlin.gradle.plugin.mpp.apple.XCFrameworkConfig
import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties
import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.compose)
    alias(libs.plugins.realm)
    alias(libs.plugins.kover)
    kotlin("native.cocoapods")
    id("kotlin-parcelize")
    id("kotlinx-serialization")
    `maven-publish`
}

val appId: String by project
val appVersionName: String by project
val localProperties = gradleLocalProperties(rootDir, providers)

group = appId
version = appVersionName

android {
    namespace = "$appId.shared"
    compileSdk = (findProperty("androidCompileSdkVersion") as String).toInt()
    defaultConfig {
        minSdk = (findProperty("androidMinSdkVersion") as String).toInt()

        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            setProguardFiles(mutableListOf("proguard-rules.pro"))
        }
        getByName("debug") {
            isMinifyEnabled = false
            enableUnitTestCoverage = true
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    buildFeatures {
        //dataBinding = true
        buildConfig = true
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.compose.compiler.get()
    }
    kotlin {
        jvmToolchain(JavaVersion.VERSION_17.toString().toInt())
    }
}

kotlin {
    androidTarget {
        compilations.all {
            kotlinOptions {
                jvmTarget = JavaVersion.VERSION_17.toString()
            }
        }
        publishLibraryVariants("release")
        publishLibraryVariantsGroupedByFlavor = false
    }

    iosX64()
    iosArm64()
    iosSimulatorArm64()

    /* fasten build
    macosX64()
    macosArm64()
    */

    /* currently breaking unit test
    jvm("desktop")
    */

    /* currently realm not supported on js
    js(IR) {
        browser()
    }
    */

    targets.configureEach {
        compilations.configureEach {
            compileTaskProvider.get().compilerOptions {
                freeCompilerArgs.add("-Xexpect-actual-classes")
            }
        }
        if (this is KotlinNativeTarget && konanTarget.family.isAppleFamily){
            compilations.getByName("main").cinterops.create("kvo"){
                packageName("$appId.shared")
            }
        }
    }

    val xcf = XCFrameworkConfig(project)
    cocoapods {
        summary = "Provide base constructor / abstract for simplify code structure"
        homepage = "https://gitlab.com/kotlin-multiplatform-mobile/core"
        version = appVersionName
        ios.deploymentTarget = findProperty("iosDeploymentTarget") as String
        framework {
            isStatic = false
            xcf.add(this)
        }
        extraSpecAttributes["resources"] = "['src/commonMain/resources/**', 'src/iosMain/resources/**']"
    }

    sourceSets {
        all {
            languageSettings {
                optIn("org.jetbrains.compose.resources.ExperimentalResourceApi")
            }
        }
        val commonMain by getting {
            dependencies {
                implementation(libs.coroutines.core)
                implementation(libs.koin.core)
                implementation(libs.koin.compose)
                implementation(libs.bundles.ktor)
                //implementation(libs.realm)
                implementation(libs.compose.navigation)
                //implementation(libs.compose.constraintlayout)
                implementation(compose.runtime)
                implementation(compose.foundation)
                implementation(compose.material3)
                implementation(compose.materialIconsExtended)
                implementation(compose.components.resources)
                implementation(compose.components.uiToolingPreview)
                implementation(libs.datastore)
                implementation(libs.coil)
                implementation(libs.coil.compose)
                implementation(libs.coil.ktor)
                implementation(libs.datetime)
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
                implementation(libs.coroutines.test)
                implementation(libs.mockk.common)
            }
        }

        val androidMain by getting {
            dependsOn(commonMain)
            dependencies {
                api(libs.timber)
                api(libs.bundles.ktx)
                api(libs.bundles.ui)
                api(libs.bundles.arch)
                api(libs.bundles.navigation)
                implementation(libs.ktor.okhttp)
                implementation(libs.koin.android)
                implementation(libs.bundles.glide)
                implementation(libs.bundles.retrofit)
                implementation(libs.lottie)
                implementation(compose.uiTooling)
                implementation(compose.preview)
                implementation(libs.coil.okhttp)
                implementation(libs.bundles.exoplayer)
            }
            resources.srcDir("./res")
        }
        val androidUnitTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
                implementation(libs.junit)
                implementation(libs.mockk)
            }
        }
        val iosX64Main by getting
        val iosArm64Main by getting
        val iosSimulatorArm64Main by getting
        val iosMain by creating {
            dependsOn(commonMain)
            iosX64Main.dependsOn(this)
            iosArm64Main.dependsOn(this)
            iosSimulatorArm64Main.dependsOn(this)
            dependencies {
                implementation(libs.ktor.darwin)
            }
        }

        /* currently breaking unit test
        val desktopMain by getting {
            dependsOn(commonMain)
            dependencies {
                api(compose.desktop.common)
                api(libs.ktor.okhttp.jvm)
                api(compose.uiTooling)
                api(compose.preview)
                api(libs.vlc)
            }
        }
        */

        /* fasten build
        val macosX64Main by getting
        val macosArm64Main by getting
        val macosMain by creating {
            dependsOn(commonMain)
            macosX64Main.dependsOn(this)
            macosArm64Main.dependsOn(this)
            dependencies {
                implementation(libs.ktor.darwin)
            }
        }
        */

        /* currently realm not supported on js
        val jsMain by getting {
            dependsOn(commonMain)
            dependencies {
                api(libs.ktor.js)
            }
        }
        */
    }

    task("testClasses")
}

publishing {
    publications {
        create<MavenPublication>("release") {
            from(components["kotlin"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/${findProperty("repoId") as String}/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = localProperties.getProperty("token")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

koverReport {
    val excs: MutableList<String> by rootProject.extra
    defaults {
        mergeWith("debug")
        verify {
            rule("Minimal line coverage rate in percents") {
                minBound(10)
            }
        }
    }
    filters {
        excludes {
            classes(*excs.toTypedArray())
        }
    }
    androidReports("release") {
        filters {
            excludes {
                classes(*excs.toTypedArray())
            }
        }
    }
}