Pod::Spec.new do |spec|
    spec.name                  = 'core_shared'
    spec.version               = '2.0.0'
    spec.homepage              = 'https://gitlab.com/kotlin-multiplatform-mobile/core'
    spec.source                = { :git => 'https://gitlab.com/kotlin-multiplatform-mobile/core.git', :tag => spec.version.to_s }
    spec.license               = { :type => 'MIT', :file => 'LICENSE' }
    spec.summary               = 'Provide base constructor / abstract for simplify code structure'
    spec.authors      		   =  { 'Hamzah Tossaro' => 'hamzah.tossaro@gmail.com' }
    spec.vendored_frameworks   = 'core_shared/build/XCFrameworks/release/core_shared.xcframework'
    spec.libraries             = 'c++'
    spec.ios.deployment_target = '14.1'
end