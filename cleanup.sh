#!/bin/sh
rm -rf .idea
./gradlew clean
rm -rf .gradle
rm -rf build
rm -rf */build
rm -rf core_ios/iosApp.xcworkspace
rm -rf core_ios/Pods
rm -rf core_ios/iosApp.xcodeproj/project.xcworkspace
rm -rf core_ios/iosApp.xcodeproj/xcuserdata
