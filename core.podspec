Pod::Spec.new do |spec|
    spec.name                  = 'core'
    spec.version               = '2.0.0'
    spec.homepage              = 'https://gitlab.com/kotlin-multiplatform-mobile/core'
    spec.source                = { :git => 'https://gitlab.com/kotlin-multiplatform-mobile/core.git', :tag => spec.version.to_s }
    spec.license               = { :type => 'MIT', :file => 'LICENSE' }
    spec.summary               = 'Provide base constructor / abstract for simplify code structure'
    spec.authors      		   =  { 'Hamzah Tossaro' => 'hamzah.tossaro@gmail.com' }
    spec.source_files 		   = "core_ios/core/Classes/**/*.{swift}"
    spec.resources             = "core_ios/core/Resources/**/*.{gif,png,jpeg,jpg,storyboard,xib,xcassets}"
    spec.libraries             = 'c++'
    spec.ios.deployment_target = '14.1'
    spec.static_framework      = true
    spec.dependency 'core_shared'
end
